/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/28                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "CalculationStrategy.h"
using namespace Mandelbrot::Core;

namespace
{
   std::vector<std::unique_ptr<CalculationStrategy>>& GetStrategies()
   {
      static std::vector<std::unique_ptr<CalculationStrategy>> __strategies;
      return __strategies;
   }
}

void CalculationStrategy::RegisterStrategy(CalculationStrategyCreator ctor)
{
   GetStrategies().push_back(std::move(ctor()));
}

std::vector<std::unique_ptr<CalculationStrategy>>& CalculationStrategy::RegisteredStrategies()
{
   return GetStrategies();
}

CalculationStrategy::CalculationStrategy(const CalculationSettings& settings)
   : m_settings(&settings)
{}

const ComplexStorage& CalculationStrategy::storage() const
{
   static ComplexStorage __empty;

   if (!m_storage)
      return __empty;

   return *m_storage;
}

void CalculationStrategy::setStorage(const ComplexStorage& storage) const
{
   m_storage = &storage;
}

void CalculationStrategy::setSettings(const CalculationSettings& settings)
{
   m_settings = &settings;
}

const CalculationSettings& CalculationStrategy::getSettings() const
{
   return *m_settings;
}
