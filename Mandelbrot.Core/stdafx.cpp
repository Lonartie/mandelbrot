/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

namespace Mandelbrot::Core::Dll
{
   bool MANDELBROTCORE_EXPORT DLL_LOAD = true;
}