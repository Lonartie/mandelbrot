/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/20                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "OclCalculationInput.h"
using namespace Mandelbrot::Core;

STRUCTURE_CLASS_CPP_BEGIN(OclCalculationInput);

STRUCTURE_FIELD_CPP(OclCalculationInput, double, rect_x1);
STRUCTURE_FIELD_CPP(OclCalculationInput, double, rect_y1);
STRUCTURE_FIELD_CPP(OclCalculationInput, double, rect_x2);
STRUCTURE_FIELD_CPP(OclCalculationInput, double, rect_y2);

STRUCTURE_FIELD_CPP(OclCalculationInput, double, offset_x);
STRUCTURE_FIELD_CPP(OclCalculationInput, double, offset_y);

STRUCTURE_FIELD_CPP(OclCalculationInput, int, width);
STRUCTURE_FIELD_CPP(OclCalculationInput, int, height);

STRUCTURE_FIELD_CPP(OclCalculationInput, int, max_iterations);

STRUCTURE_FIELD_CPP(OclCalculationInput, double, pdistance);

STRUCTURE_CLASS_CPP_END(OclCalculationInput);