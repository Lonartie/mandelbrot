/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexRect.h"

using namespace Mandelbrot::Core;

ComplexRect::ComplexRect(const ComplexPoint& upperLeft, const ComplexPoint& lowerRight)
   : m_upperLeft(upperLeft)
   , m_lowerRight(lowerRight)
{}

ComplexRect::type ComplexRect::calculateWidth() const
{
   return std::abs(m_lowerRight.real() - m_upperLeft.real());
}

ComplexRect::type ComplexRect::calculateHeight() const
{
   return std::abs(m_upperLeft.imag() - m_lowerRight.imag());
}

ComplexRect& ComplexRect::setUpperLeft(const ComplexPoint& upperLeft)
{
   m_upperLeft = upperLeft;
   return *this;
}

const ComplexPoint& ComplexRect::getUpperLeft() const
{
   return m_upperLeft;
}

ComplexRect& ComplexRect::setLowerRight(const ComplexPoint& lowerRight)
{
   m_lowerRight = lowerRight;
   return *this;
}

const ComplexPoint& ComplexRect::getLowerRight() const
{
   return m_lowerRight;
}

ComplexRect::type ComplexRect::top() const
{
   return m_upperLeft.imag();
}

ComplexRect::type ComplexRect::right() const
{
   return m_lowerRight.real();
}

ComplexRect::type ComplexRect::left() const
{
   return m_upperLeft.real();
}

ComplexRect::type ComplexRect::bottom() const
{
   return m_lowerRight.imag();
}

ComplexRect& ComplexRect::operator=(const ComplexRect& o)
{
   m_upperLeft = o.m_upperLeft;
   m_lowerRight = o.m_lowerRight;
   return *this;
}

bool ComplexRect::operator==(const ComplexRect& o) const noexcept
{
   return m_upperLeft == o.m_upperLeft
      && m_lowerRight == o.m_lowerRight;
}

bool ComplexRect::operator!=(const ComplexRect& o) const noexcept
{
   return !operator==(o);
}
