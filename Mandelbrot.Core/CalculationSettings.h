/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/28                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

#include "SmoothingStrategy.h"

namespace Mandelbrot::Core
{
   /// @brief           storing information like max iterations, smoothing algorithm and interpolation algorithm
   struct MANDELBROTCORE_EXPORT CalculationSettings
   {
      
      CalculationSettings(std::size_t maxIterations, SmoothingStrategy& smoothingAlgorithm);
      
      /// @brief        how many iterations may be calculated before the point is considered converging
      std::size_t MaxIterations = 0;

      /// @brief <<<
      SmoothingStrategy* SmoothingAlgorithm;
   };
}
