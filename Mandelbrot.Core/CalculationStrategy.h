/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexDataSet.h"
#include "CalculationSettings.h"
#include "ComplexStorage.h"

namespace Mandelbrot::Core
{
   /// @brief                    an interface for various calculation type for both mandelbrot and julia set
   class MANDELBROTCORE_EXPORT CalculationStrategy
   {
   public: /*typedefs*/

      /// @brief                 the type all calculations are based on (e.g. float)
      using type = precision_type;
      using CalculationStrategyCreator = std::function<std::unique_ptr<CalculationStrategy>()>;

   public: /*static methods*/

      /// @brief                 registers a strategy through a constructor function
      /// @param ctor            the constructor function for registration
      static void RegisterStrategy(CalculationStrategyCreator ctor);

      /// @brief                 get all registered strategies
      static std::vector<std::unique_ptr<CalculationStrategy>>& RegisteredStrategies();

   public: /*methods*/
      
      /// @brief                 standard constructor
      /// @note                  initialize the CalculationSettings through setSettings(const CalculationSettings&) !
      CalculationStrategy() = default;

      ///@brief                  constructor with settings initialized
      CalculationStrategy(const CalculationSettings& settings);

      CalculationStrategy(const CalculationStrategy&) = delete;
      CalculationStrategy(CalculationStrategy&&) = delete;
      virtual ~CalculationStrategy() = default;

      /// @brief                 set the calculation settings (such as max iterations, smoothing algorithm and interpolation algorithm)
      /// @param settings        the settings used to calculate
      void setSettings(const CalculationSettings& settings);

      /// @brief                 get the settings used to calculate the iterations
      /// @returns               the settings object storing all that information
      const CalculationSettings& getSettings() const;

      /// @brief                 set the storage object to check whether or not a point has already been calculated (performance)
      /// @param storage         the new storage object
      void setStorage(const ComplexStorage& storage) const;

      /// @brief                 get the storage object to check whether or not a point has already been calculated (performance)
      /// @returns               the storage object
      const ComplexStorage& storage() const;

      /// @brief                 calculate the fractal series of mandelbrot / julia
      /// @param rect            the complex rect that should be calculated
      /// @param pDistance       the distance between two points
      /// @param offset          the point calculation offset (used for the julia set)
      virtual ComplexDataSet calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const = 0;

      /// @brief                get the name of the strategy
      /// @returns              the name of the strategy              
      virtual QString getName() const = 0;

   private: /*members*/

      /// @brief                 the settings object
      const CalculationSettings* m_settings = nullptr;

      /// @brief                 the storage object (performance)
      mutable const ComplexStorage* m_storage = nullptr;

   };
}


#define REGISTER_CALCULATION_STRATEGY(CLASS, ...)                 \
namespace                                                         \
{                                                                 \
   bool STRATEGY_REGISTERED = []()                                \
   {                                                              \
      Mandelbrot::Core::CalculationStrategy::RegisterStrategy([]()\
      {                                                           \
         return std::make_unique<CLASS>(__VA_ARGS__);             \
      });                                                         \
      return true;                                                \
   }();                                                           \
}
