/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Felix Germershausen                     */
/*  Date    :   2020/04/02                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

#include "stdafx.h"

namespace Mandelbrot::Core
{
   /// @brief					 a strategy for interpolating unknown values between known ones
   class MANDELBROTCORE_EXPORT InterpolationStrategy
   {
   public: /*typedefs*/

      using itype = iteration_type;

   public: /*methods*/

      virtual itype interpolate(const std::vector<itype>& neighbours, precision_type x, precision_type y) const = 0;

      virtual std::size_t neighbourSize() const = 0;
   };
}
