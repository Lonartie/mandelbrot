/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexPlaneMapper.h"

using namespace Mandelbrot::Core;

const ComplexRect& ComplexPlaneMapper::getVisibleRect() const
{
	return m_visibleRect;
}

ComplexPlaneMapper::type ComplexPlaneMapper::calculatePointDistance() const
{
	return m_visibleRect.calculateWidth() / m_displaySize.width();
}

void ComplexPlaneMapper::setVisibleRect(const ComplexRect& rect, const QSizeF& viewSize)
{
	m_visibleRect = rect;
	m_displaySize = viewSize;
}

ComplexRect ComplexPlaneMapper::calculateVisibleRect(const QRectF& scene, const QRectF& viewport, const ComplexRect& lastRect)
{
	const auto x1_off = lastRect.calculateWidth() / static_cast<precision_type>(scene.size().width()) * static_cast<precision_type>(viewport.left());
	const auto y1_off = lastRect.calculateHeight() / static_cast<precision_type>(scene.size().height()) * static_cast<precision_type>(viewport.top());

	const auto x2_off = lastRect.calculateWidth() / static_cast<precision_type>(scene.size().width()) * static_cast<precision_type>(viewport.left() + viewport.width());
	const auto y2_off = lastRect.calculateHeight() / static_cast<precision_type>(scene.size().height()) * static_cast<precision_type>(viewport.top() + viewport.height());

	const auto topLeft = ComplexPoint(lastRect.left() + x1_off, lastRect.top() - y1_off);
	const auto bottomRight = ComplexPoint(lastRect.left() + x2_off, lastRect.top() - y2_off);

	return ComplexRect(topLeft, bottomRight);
}

ComplexPoint ComplexPlaneMapper::translate(const ViewPoint& /*viewPoint*/) const
{
	return {};
}

ViewPoint ComplexPlaneMapper::translate(const ComplexPoint& /*complexPoint*/) const
{
	return {};
}
