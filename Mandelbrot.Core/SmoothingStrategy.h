/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexPoint.h"

namespace Mandelbrot::Core
{
   /// @brief                    provides strategies to smooth a certain value in a mandelbrot / julia calculation
   class MANDELBROTCORE_EXPORT SmoothingStrategy
   {
   public: /*typedefs*/

      using itype = iteration_type;
   
   public: /*methods*/

      virtual ~SmoothingStrategy() = default;

      /// @brief                 smooth the iterations value while calculating
      /// @note                  use like "iterations = smoothUpdate(...);"
      /// @param currentPoint    the current complex value / point on complex plane
      /// @param iterations      the current iterations that happened (may already be smoothed)
      /// @param maxIterations   the maximum iterations that may happen
      /// @returns               the smoothed iteration count
      virtual itype smoothUpdate(const ComplexPoint& currentPoint, itype iterations, std::size_t maxIterations) const;

      /// @brief                 smooth the iterations value at the end of the calculation
      /// @note                  use like "iterations = smoothEnd(...);"
      /// @param endPoint        the current complex value / point on complex plane
      /// @param iterations      the current iterations that happened (may already be smoothed)
      /// @param maxIterations   the maximum iterations that may happen
      /// @returns               the smoothed iteration count
      virtual itype smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const;

   };
}
