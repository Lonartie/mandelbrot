/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/16                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "MandelbrotOCLStrategy.h"

using namespace Mandelbrot::Core;

REGISTER_CALCULATION_STRATEGY(MandelbrotOCLStrategy);

KERNEL_CLASS_CPP_BEGIN(MandelbrotOCLStrategy);

MandelbrotOCLStrategy::MandelbrotOCLStrategy(const EasyOCL::OCLDevice& device)
   : CalculationStrategy()
   , m_device(device)
{}

MandelbrotOCLStrategy::MandelbrotOCLStrategy(const EasyOCL::OCLDevice& device, const CalculationSettings& settings)
   : CalculationStrategy(settings)
   , m_device(device)
{}

QString MandelbrotOCLStrategy::getName() const
{
   return "Mandelbrot OpenCL Strategy (GPU-accelerated massive multi-threading)";
}

ComplexDataSet MandelbrotOCLStrategy::calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const
{
   ComplexDataSet out(rect, pDistance);

   std::vector<double> res;
   res.resize(out.size());
   OclCalculationInput in;

   in.rect_x1 = static_cast<double>(rect.getUpperLeft().real());
   in.rect_y1 = static_cast<double>(rect.getUpperLeft().imag());
   in.rect_x2 = static_cast<double>(rect.getLowerRight().real());
   in.rect_y2 = static_cast<double>(rect.getLowerRight().imag());
   in.offset_x = static_cast<double>(offset.real());
   in.offset_y = static_cast<double>(offset.imag());
   in.pdistance = static_cast<double>(pDistance);
   in.width = out.dim().width();
   in.height = out.dim().height();
   in.max_iterations = getSettings().MaxIterations;

   auto result = kernelCalculate(m_device, out.size(), {&in, 1}, res);

   if (result.errorType != EasyOCL::NO_CL_ERROR)
   {
      __debugbreak();
   }

   std::transform(res.begin(), res.end(), out.data().begin(), [](double val) { return static_cast<precision_type>(val); });
   return out;
}

KERNEL_FUNCTION_CPP(MandelbrotOCLStrategy, kernelCalculate, (const OclCalculationInput*, in, double*, out),
{
   const int index = EasyOCL::get_global_id(0);

   const double rect_x1 = in->rect_x1;
   const double rect_y1 = in->rect_y1;
   const double pdistance = in->pdistance;
   const int max_iterations = in->max_iterations;
   const int width = in->width;

   const int x = index % width;
   const int y = index / width;

   double z_real = 0;
   double z_imag = 0;

   double c_real = rect_x1 + (pdistance * x);
   double c_imag = rect_y1 - (pdistance * y);

   double iterations_took = 0;

   double zr = 0;
   double zi = 0;

   for (; iterations_took < max_iterations; iterations_took = iterations_took + 1)
   {
      zr = z_real;
      zi = z_imag;

      z_real = zr * zr - zi * zi + c_real;
      z_imag = 2 * zr * zi + c_imag;

      if (sqrt(z_real * z_real + z_imag * z_imag) > 2)
      {
         break;
      }
   }

   out[index] = iterations_took;
}, OclCalculationInput);

KERNEL_CLASS_CPP_END(MandelbrotOCLStrategy);