/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/08                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexPoint.h"
#include "ComplexDataSet.h"

namespace Mandelbrot::Core
{
   class MANDELBROTCORE_EXPORT ComplexStorage final
   {
   public: /*typedefs*/

      using ptype = precision_type;
      using itype = iteration_type;

   public: /*ctors*/

      ComplexStorage() = default;
      ~ComplexStorage() = default;

   public: /*methods*/
      
      bool isDataPointCalculated(const ComplexPoint& point) const;
      precision_type getDataPoint(const ComplexPoint& point) const;

      void addDataSet(const ComplexDataSet& set);
      void addDataPoint(const ComplexPoint& point, itype iterations);

   private: /*members*/

      std::map<ComplexPoint, ptype> m_system;

   };
}
