/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "SmoothingStrategy.h"

namespace Mandelbrot::Core
{
   /// @brief              provides a strategy where the iterations will NOT be smoothed (output = input)
   class MANDELBROTCORE_EXPORT NoSmoothingStrategy: public SmoothingStrategy
   {
   public: /*implementation from SmoothingStrategy*/
    
      /// @copydoc Mandelbrot::Core::SmoothingStrategy::smoothUpdate(const ComplexPoint&, itype, std::size_t)
      itype smoothUpdate(const ComplexPoint& currentPoint, itype iterations, std::size_t maxIterations) const override;

      /// @copydoc Mandelbrot::Core::SmoothingStrategy::smoothEnd(const ComplexPoint&, itype, std::size_t)
      itype smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const override;
   };
}
