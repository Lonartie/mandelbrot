/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

// Mandelbrot.Core
#include "MandelbrotCoreLib.h"
#include "Typedefs.h"

// QtCore
#include <QObject>
#include <QString>
#include <QStringList>
#include <QSize>
#include <QSizeF>
#include <QRect>
#include <QRectF>
#include <QThread>

// stl
#include <utility>
#include <memory>
#include <vector>
#include <map>
#include <functional>
#include <stdexcept>
#include <complex.h>
