/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

namespace Mandelbrot::Core
{
   using ComplexPoint = std::complex<precision_type>;
}

namespace std
{
   /// @brief           compare two complex points with lexicographic order
   inline bool operator<(const Mandelbrot::Core::ComplexPoint& a, const Mandelbrot::Core::ComplexPoint& b)
   {
      if (a.real() != b.real())
         return a.real() < b.real();
      return a.imag() < b.imag();
   }

   /// @brief           compare two complex points with lexicographic order
   inline bool operator>(const Mandelbrot::Core::ComplexPoint& a, const Mandelbrot::Core::ComplexPoint& b)
   {
      if (a.real() != b.real())
         return a.real() > b.real();
      return a.imag() > b.imag();
   }

   /// @brief           compare two complex points with lexicographic order
   inline bool operator<=(const Mandelbrot::Core::ComplexPoint& a, const Mandelbrot::Core::ComplexPoint& b)
   {
      return operator<(a, b) || operator==(a, b);
   }

   /// @brief           compare two complex points with lexicographic order
   inline bool operator>=(const Mandelbrot::Core::ComplexPoint& a, const Mandelbrot::Core::ComplexPoint& b)
   {
      return operator>(a, b) || operator==(a, b);
   }
}