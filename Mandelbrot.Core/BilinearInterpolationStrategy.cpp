/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/17                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "BilinearInterpolationStrategy.h"
using namespace Mandelbrot::Core;

InterpolationStrategy::itype BilinearInterpolationStrategy::interpolate(const std::vector<itype>& neighbours, precision_type x, precision_type y) const
{
   if (neighbours.size() != 4) return 0;
   if (x < 0 || x > 1) return 0;
   if (y < 0 || y > 1) return 0;

   const auto x1 = linearInterpolate(neighbours[0], neighbours[1], x);
   const auto x2 = linearInterpolate(neighbours[2], neighbours[3], x);

   return linearInterpolate(x1, x2, y);
}

std::size_t BilinearInterpolationStrategy::neighbourSize() const
{
   return 1;
}

InterpolationStrategy::itype BilinearInterpolationStrategy::linearInterpolate(itype left, itype right, precision_type x)
{
   return left + (right - left) * x;
}
