/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/20                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include <EasyOCL>

namespace Mandelbrot::Core
{
   struct OclCalculationInput
   {
      STRUCTURE_CLASS_H(OclCalculationInput);
   
   public:

      STRUCTURE_FIELD_H(double, rect_x1 = 0);
      STRUCTURE_FIELD_H(double, rect_y1 = 0);
      STRUCTURE_FIELD_H(double, rect_x2 = 0);
      STRUCTURE_FIELD_H(double, rect_y2 = 0);

      STRUCTURE_FIELD_H(double, offset_x = 0);
      STRUCTURE_FIELD_H(double, offset_y = 0);

      STRUCTURE_FIELD_H(int, width = 0);
      STRUCTURE_FIELD_H(int, height = 0);

      STRUCTURE_FIELD_H(int, max_iterations = 0);

      STRUCTURE_FIELD_H(double, pdistance = 0);

   };
}
