/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/28                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexDataSet.h"
using namespace Mandelbrot::Core;

ComplexDataSet::ComplexDataSet(const ComplexRect& rect, ptype pDistance)
{
   m_rect = rect;
   m_pDistance = pDistance;
   calculateSize();
}

void ComplexDataSet::setPointDistance(ptype pDistance)
{
   m_pDistance = pDistance;
   calculateSize();
}

std::size_t ComplexDataSet::size() const
{
   return m_size;
}

void ComplexDataSet::setRect(const ComplexRect& rect)
{
   m_rect = rect;
   calculateSize();
}

void ComplexDataSet::setDataPoint(const ComplexPoint& point, itype iterations)
{
	const auto index = calculateIndex(point);
   
   m_data[index] = iterations;
}

void ComplexDataSet::setDataPoint(std::size_t index, itype iterations)
{
    m_data[index] = iterations;
}

void ComplexDataSet::calculateSize()
{
   if (m_pDistance <= 0)
   {
      m_size = 0;
      return;
   }

   const auto rwidth = m_rect.calculateWidth();
   const auto rheight = m_rect.calculateHeight();

   const auto width = static_cast<std::size_t>(std::floor(rwidth / m_pDistance)) + 1; // +1 to match start AND end of the sequence (begin and end including)
   const auto height = static_cast<std::size_t>(std::floor(rheight / m_pDistance)) + 1; // +1 to match start AND end of the sequence (begin and end including)

   m_dimensions.setWidth(width);
   m_dimensions.setHeight(height);

   m_size = width * height;
   
   // IMPORTANT: resize the vector!
   m_data.resize(m_size);
}

std::size_t ComplexDataSet::calculateIndex(const ComplexPoint& point) const
{
	const auto rectWidth = m_rect.calculateWidth();
	const auto rectHeight = m_rect.calculateHeight();

   if (rectWidth <= 0 || rectHeight <= 0)
   {
      return 0;
   }

	const auto cx = point.real() - m_rect.left();
	const auto cy = m_rect.top() - point.imag();

	const auto x = static_cast<std::size_t>(std::round(cx / rectWidth * (m_dimensions.width() - 1)));
	const auto y = static_cast<std::size_t>(std::round(cy / rectHeight * (m_dimensions.height() - 1)));

	const auto index = y * m_dimensions.width() + x;
   return index;
}

std::vector<ComplexDataSet::itype>& ComplexDataSet::data()
{
   return m_data;
}

bool ComplexDataSet::operator==(const ComplexDataSet& o) const noexcept
{
   if (m_rect != o.m_rect) return false;
   if (m_pDistance != o.m_pDistance) return false;
   if (m_size != o.m_size) return false;
   if (m_dimensions != o.m_dimensions) return false;

   return std::equal(m_data.begin(), m_data.end(),
                     o.m_data.begin(), o.m_data.end());
}

bool ComplexDataSet::operator!=(const ComplexDataSet& o) const noexcept
{
   return !operator==(o);
}

std::map<ComplexPoint, ComplexDataSet::itype> ComplexDataSet::dataMap() const
{
   std::map<ComplexPoint, itype> out;
   const auto rwidth = m_rect.calculateWidth();
   const auto rheight = m_rect.calculateHeight();
   for (std::size_t n = 0; n < m_dimensions.width() * m_dimensions.height(); n++)
   {
      ComplexPoint p = {
	      (n % m_dimensions.width() * rwidth / (m_dimensions.width() - 1) + m_rect.left()), // real part
	      (n / m_dimensions.height() * rheight / (m_dimensions.height() - 1) + m_rect.bottom()) // imag part
      };
      out.emplace(p, getDataPoint(n));
   }

   return out;
}

const QSize& ComplexDataSet::dim() const
{
   return m_dimensions;
}

const ComplexRect& ComplexDataSet::rect() const
{
   return m_rect;
}

ComplexDataSet::ptype ComplexDataSet::pointDistance() const
{
   return m_pDistance;
}

const std::vector<ComplexDataSet::itype>& ComplexDataSet::data() const
{
   return m_data;
}

ComplexDataSet::itype ComplexDataSet::getDataPoint(const ComplexPoint& point) const
{
	const auto index = calculateIndex(point);
   return m_data[index];
}

ComplexDataSet::itype ComplexDataSet::getDataPoint(std::size_t index) const
{
   return m_data[index];
}

