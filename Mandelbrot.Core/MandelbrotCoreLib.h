/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

#ifndef BUILD_STATIC
# if defined(MANDELBROTCORE_LIB)
#  define MANDELBROTCORE_EXPORT __declspec(dllexport)
# else
#  define MANDELBROTCORE_EXPORT __declspec(dllimport)
# endif
#else
# define MANDELBROTCORE_EXPORT
#endif
