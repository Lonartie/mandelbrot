/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/04/23                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "CalculationStrategy.h"
#include "OclCalculationInput.h"

#include <EasyOCL>

namespace Mandelbrot::Core
{
    /// @brief                     calculation of the julia set with openCL
    class MANDELBROTCORE_EXPORT JuliaOCLStrategy : public CalculationStrategy
    {
        KERNEL_CLASS_H(JuliaOCLStrategy);

    public: /*methods*/

        JuliaOCLStrategy(const EasyOCL::OCLDevice& device);
        JuliaOCLStrategy(const EasyOCL::OCLDevice& device, const CalculationSettings& settings);
        JuliaOCLStrategy();
        virtual ~JuliaOCLStrategy() = default;

        /// @copydoc Mandelbrot::Core::CalculationStrategy::getName() const
        QString getName() const override;

        /// @brief                 calculate the julia set (with openCL)
        /// @param rect            the complex rect that should be calculated
        /// @param pDistance       the distance between two points
        /// @param offset          the point calculation offset (used for the julia set)
        ComplexDataSet calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const override;

        KERNEL_FUNCTION_H(kernelCalculate, (const OclCalculationInput*, in, double*, out));

    private:

        EasyOCL::OCLDevice m_device;
    };
}
