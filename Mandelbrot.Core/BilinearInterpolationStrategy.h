/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/17                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "InterpolationStrategy.h"

namespace Mandelbrot::Core
{
   class MANDELBROTCORE_EXPORT BilinearInterpolationStrategy : public InterpolationStrategy
   {
   public:
	   itype interpolate(const std::vector<itype>& neighbours, precision_type x, precision_type y) const override;
	   std::size_t neighbourSize() const override;

   private:
      static itype linearInterpolate(itype left, itype right, precision_type x);
   };
}