#include "stdafx.h"
#include "NearestNeighbourStrategy.h"
using namespace Mandelbrot::Core;

InterpolationStrategy::itype NearestNeighbourStrategy::interpolate(const std::vector<itype>& neighbours, precision_type x, precision_type y) const
{
   if (neighbours.size() != 4) return 0;
   if (x < 0 || x > 1) return 0;
   if (y < 0 || y > 1) return 0;

   const int rx = std::round(x);
   const int ry = std::round(y);

   const int index = rx + ry;

   return neighbours[index];
}

std::size_t NearestNeighbourStrategy::neighbourSize() const
{
   return 1;
}
