/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Felix Germershausen                     */
/*  Date    :   2020/04/02                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.Core
#include "InterpolationStrategy.h"

using namespace Mandelbrot::Core;