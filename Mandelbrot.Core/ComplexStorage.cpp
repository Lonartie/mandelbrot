/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/08                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexStorage.h"
using namespace Mandelbrot::Core;

void ComplexStorage::addDataSet(const ComplexDataSet& set)
{
   for (auto& item : set.dataMap())
      addDataPoint(item.first, item.second);
}

bool ComplexStorage::isDataPointCalculated(const ComplexPoint& point) const
{
   return m_system.find(point) != m_system.end();
}

precision_type ComplexStorage::getDataPoint(const ComplexPoint& point) const
{
   if (!isDataPointCalculated(point))
      return {};

   return m_system.at(point);
}

void ComplexStorage::addDataPoint(const ComplexPoint& point, itype iterations)
{
   if (!isDataPointCalculated(point))
      m_system.emplace(point, iterations);
}
