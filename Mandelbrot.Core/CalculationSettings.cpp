/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Felix Germershausen                     */
/*  Date    :   2020/04/02                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "CalculationSettings.h"

Mandelbrot::Core::CalculationSettings::CalculationSettings(std::size_t maxIterations, SmoothingStrategy& smoothingAlgorithm)
   : MaxIterations(maxIterations)
   , SmoothingAlgorithm(&smoothingAlgorithm)
{
}

