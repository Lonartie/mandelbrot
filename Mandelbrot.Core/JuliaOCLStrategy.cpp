/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/04/23                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "JuliaOCLStrategy.h"
using namespace Mandelbrot::Core;

REGISTER_CALCULATION_STRATEGY(JuliaOCLStrategy);

KERNEL_CLASS_CPP_BEGIN(JuliaOCLStrategy);

JuliaOCLStrategy::JuliaOCLStrategy(const EasyOCL::OCLDevice& device)
    : m_device(device)
    , CalculationStrategy()
{}

JuliaOCLStrategy::JuliaOCLStrategy(const EasyOCL::OCLDevice& device, const CalculationSettings& settings)
    : m_device(device)
    , CalculationStrategy(settings)
{}

JuliaOCLStrategy::JuliaOCLStrategy()
{}

QString JuliaOCLStrategy::getName() const
{
    return "Julia OpenCL Strategy (GPU-accelerated massive multi-threading)";
}

ComplexDataSet JuliaOCLStrategy::calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const
{
    ComplexDataSet out(rect, pDistance);

    std::vector<double> res;
    res.resize(out.size());
    OclCalculationInput in;

    in.rect_x1 = (double)rect.getUpperLeft().real();
    in.rect_y1 = (double)rect.getUpperLeft().imag();
    in.rect_x2 = (double)rect.getLowerRight().real();
    in.rect_y2 = (double)rect.getLowerRight().imag();
    in.offset_x = (double)offset.real();
    in.offset_y = (double)offset.imag();
    in.pdistance = (double)pDistance;
    in.width = out.dim().width();
    in.height = out.dim().height();
    in.max_iterations = getSettings().MaxIterations;

    auto result = kernelCalculate(m_device, out.size(), { &in, 1 }, res);

    if (result.errorType != EasyOCL::NO_CL_ERROR)
    {
        __debugbreak();
    }

    std::transform(res.begin(), res.end(), out.data().begin(), [](double val) { return (precision_type)val; });
    return out;
}

KERNEL_FUNCTION_CPP(JuliaOCLStrategy, kernelCalculate, (const OclCalculationInput*, in, double*, out),
    {
       const int index = EasyOCL::get_global_id(0);

       const double offsetX = in->offset_x;
       const double offsetY = in->offset_y;
       const double distance = in->pdistance;
       const int maxIterations = in->max_iterations;
       const int width = in->width;

       const int x = index % width;
       const int y = index / width;

       double zReal = in->rect_x1 + (distance * x);
       double zImag = in->rect_y1 - (distance * y);

       double zr = 0;
       double zi = 0;

       double iterations = 0;
       for (; iterations < maxIterations; iterations = iterations + 1)
       {
          zr = zReal;
          zi = zImag;

          zReal = zr * zr - zi * zi + offsetX;
          zImag = 2 * zr * zi + offsetY;

          if (sqrt(zReal * zReal + zImag * zImag) > 2)
          {
             break;
          }
       }

       out[index] = iterations;
    }, OclCalculationInput);

KERNEL_CLASS_CPP_END(MandelbrotOCLStrategy);