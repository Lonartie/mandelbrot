/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "SmoothingStrategy.h"
using namespace Mandelbrot::Core;

SmoothingStrategy::itype SmoothingStrategy::smoothUpdate(const ComplexPoint& currentPoint, itype iterations, std::size_t maxIterations) const
{
   return iterations;
}

SmoothingStrategy::itype SmoothingStrategy::smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const
{
   return iterations;
}
