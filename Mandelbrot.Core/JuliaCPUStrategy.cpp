/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/30                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "JuliaCPUStrategy.h"

REGISTER_CALCULATION_STRATEGY(Mandelbrot::Core::JuliaCPUStrategy);

Mandelbrot::Core::ComplexDataSet Mandelbrot::Core::JuliaCPUStrategy::calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const
{
   ComplexDataSet dataSet(rect, pDistance);

   for (long long n = 0; n < dataSet.size(); n++)
   {
	   const auto x = n % dataSet.dim().width();
	   const auto y = n / dataSet.dim().width();

      ComplexPoint c(offset);
      ComplexPoint z(rect.left() + x * pDistance, rect.top() - y * pDistance);

      iteration_type iterations = 0;
      for (; iterations < getSettings().MaxIterations; iterations++)
      {
         iterations = getSettings().SmoothingAlgorithm->smoothUpdate(n, iterations, getSettings().MaxIterations);
         z = z * z + c;
         if (std::abs(z) > 2)
         {
            break;
         }
      }

      dataSet.setDataPoint(n, getSettings().SmoothingAlgorithm->smoothEnd(c, iterations, getSettings().MaxIterations));
   }

   return dataSet;
}

QString Mandelbrot::Core::JuliaCPUStrategy::getName() const
{
    return "Julia CPU Strategy (single-threaded)";
}
