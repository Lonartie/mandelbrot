/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/04/30                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "CalculationStrategy.h"

namespace Mandelbrot::Core
{
    /// @brief                     calculation of the julia set with threading
    class MANDELBROTCORE_EXPORT JuliaCPUMultiStrategy : public CalculationStrategy
    {
    public: /*methods*/

        using CalculationStrategy::CalculationStrategy;

        /// @brief                 calculate the julia set (multi-threaded)
        /// @param rect            the complex rect that should be calculated
        /// @param pDistance       the distance between two points
        /// @param offset          the point calculation offset (used for the julia set)
        ComplexDataSet calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const override;

        //// @copydoc Mandelbrot::Core::CalculationStrategy::getName() const
        QString getName() const override;
    };
}
