/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/16                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "CalculationStrategy.h"
#include "OclCalculationInput.h"

#include <EasyOCL>

namespace Mandelbrot::Core
{
   /// @brief                     calculation of the mandelbrot set without threading
   class MANDELBROTCORE_EXPORT MandelbrotOCLStrategy: public CalculationStrategy
   {
      KERNEL_CLASS_H(MandelbrotOCLStrategy);

   public: /*methods*/

      MandelbrotOCLStrategy(const EasyOCL::OCLDevice& device);
      MandelbrotOCLStrategy(const EasyOCL::OCLDevice& device, const CalculationSettings& settings);
      MandelbrotOCLStrategy() = default;
      virtual ~MandelbrotOCLStrategy() = default;

      /// @copydoc Mandelbrot::Core::CalculationStrategy::getName() const
      QString getName() const override;

      /// @brief                 calculate the mandelbrot set (single-threaded)
      /// @param rect            the complex rect that should be calculated
      /// @param pDistance       the distance between two points
      /// @param offset          the point calculation offset (used for the julia set)
      ComplexDataSet calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& offset) const override;

      KERNEL_FUNCTION_H(kernelCalculate, (const OclCalculationInput*, in, double*, out));

   private:

      EasyOCL::OCLDevice m_device;
   };
}
