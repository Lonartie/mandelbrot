/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexPoint.h"
#include "ComplexRect.h"

namespace Mandelbrot::Core
{
   using ViewPoint = QPointF;

   /// @brief                 translates a fractal map from map space to view space
   class MANDELBROTCORE_EXPORT ComplexPlaneMapper
   {
   public: /*typedefs*/

      using type = precision_type;

   public: /*ctors*/

      ComplexPlaneMapper() = default;
      ComplexPlaneMapper(const ComplexPlaneMapper&) = delete;
      ComplexPlaneMapper(ComplexPlaneMapper&&) = delete;

   public: /*static*/

      /// @brief <<<
      static ComplexRect calculateVisibleRect(const QRectF& scene, const QRectF& viewport, const ComplexRect& lastRect);

   public: /*methods*/

      ComplexPoint translate(const ViewPoint& viewPoint) const;

      ViewPoint translate(const ComplexPoint& complexPoint) const;

      /// @brief <<<
      const ComplexRect& getVisibleRect() const;

      /// @brief <<<            
      void setVisibleRect(const ComplexRect& rect, const QSizeF& viewSize);

      /// @brief <<<
      type calculatePointDistance() const;

   private: /*members*/

      ComplexRect m_visibleRect;
      QSizeF m_displaySize;

   };
}