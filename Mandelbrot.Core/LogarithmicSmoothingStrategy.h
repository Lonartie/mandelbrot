/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/21                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "SmoothingStrategy.h"

namespace Mandelbrot::Core
{
   class MANDELBROTCORE_EXPORT LogarithmicSmoothingStrategy : public SmoothingStrategy
   {
   public:

	   itype smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const override;

   };
}