/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexPlane.h"

using namespace Mandelbrot::Core;

ComplexPlane::ComplexPlane(const CalculationStrategy& strategy, const ComplexPlaneMapper& mapper)
   : m_strategy(&strategy)
   , m_mapper(&mapper)
{
   m_strategy->setStorage(m_dataStorage);
}

ComplexPlane::ComplexPlane(const ComplexPlaneMapper& mapper)
   : m_mapper(&mapper)
{}

void ComplexPlane::calculate(const QPoint& mousePos /*= {-1,-1}*/)
{
	const auto rect = m_mapper->getVisibleRect();
   auto pdis = m_mapper->calculatePointDistance();
	const auto offs = m_mapper->translate(ViewPoint(mousePos));

   if (m_interpolEnabled && m_interpolation != nullptr)
   {
	   pdis = pdis * static_cast<precision_type>(std::pow(2, m_interpolationLevel));
	}

	auto out = m_strategy->calculate(rect, pdis, offs);

   if (m_interpolEnabled && m_interpolation != nullptr)
   {
	   out = interpolateValues(out);
	}

	emit calculationDone(out);
}

void ComplexPlane::setCalculationStrategy(const CalculationStrategy& strategy)
{
   m_strategy = &strategy;
   m_strategy->setStorage(m_dataStorage);
}

void ComplexPlane::setComplexPlaneMapper(const ComplexPlaneMapper& mapper)
{
   m_mapper = &mapper;
}

const CalculationStrategy& ComplexPlane::calculationStrategy() const
{
   return *m_strategy;
}

void ComplexPlane::setInterpolationStrategy(const InterpolationStrategy& interpol)
{
   m_interpolation = &interpol;
}

void ComplexPlane::enableInterpolation(bool enabled)
{
   m_interpolEnabled = enabled;
}

bool ComplexPlane::isInterpolationEnabled() const
{
   return m_interpolEnabled;
}

void ComplexPlane::setInterpolationLevel(int level)
{
   if (level < 1 || level > 3)
   {
	   return;
   }
   m_interpolationLevel = level;
}

int ComplexPlane::interpolationLevel() const
{
   return m_interpolationLevel;
}

ComplexDataSet ComplexPlane::interpolateValues(const ComplexDataSet& set) const
{
   const int spacing = std::pow(2, m_interpolationLevel);
   const int nsize = m_interpolation->neighbourSize();
   ComplexDataSet out(set.rect(), set.pointDistance() / static_cast<float>(spacing));

   // filling the known values
#pragma omp parallel for
   for (auto x = 0; x < set.dim().width(); x++)
   {
      for (auto y = 0; y < set.dim().height(); y++)
      {
	      const auto in_index = y * set.dim().width() + x;
	      const auto out_index = y * spacing * out.dim().width() + x * spacing;
         out.setDataPoint(out_index, set.getDataPoint(in_index));
      }
   }

   // interpolating the missing values
#pragma omp parallel for
   for (long long n = 0; n < set.dim().width() * set.dim().height(); n++)
   {
      const std::size_t x = n % set.dim().width();
      const std::size_t y = n / set.dim().width();

      if (x == 0 && y == 0)
      {
	      continue;
      }

      const auto cx = x * spacing;
      const auto cy = y * spacing;

      std::vector<iteration_type> neighbours;
      for (auto _y = -nsize; _y <= nsize; _y++)
      {
         for (auto _x = -nsize; _x <= nsize; _x++)
         {
            if (_y == 0 || _x == 0)
            {
	            continue;
            }

            const auto tx = cx - 1 + _x;
            const auto ty = cy - 1 + _y;

            // all values that does not exist (edge cases) will be set to 0!
            if (tx < 0 || tx >= out.dim().width()) { neighbours.push_back(0); continue; }
            if (ty < 0 || ty >= out.dim().height()) { neighbours.push_back(0); continue; }

            const auto out_index = ty * out.dim().width() + tx;
            neighbours.push_back(out.getDataPoint(out_index));
         }
      }

      // left
      if (x > 0)
      {
	      const auto ipolated = m_interpolation->interpolate(neighbours, 0.5, 1.0);
         out.setDataPoint(cy *out.dim().width() + (cx - 1), ipolated);
      }

      // top
      if (y > 0)
      {
	      const auto ipolated = m_interpolation->interpolate(neighbours, 1.0, 0.5);
         out.setDataPoint((cy - 1) * out.dim().width() + cx, ipolated);
      }

      // middle
      if (x > 0 && y > 0)
      {
	      const auto ipolated = m_interpolation->interpolate(neighbours, 0.5, 0.5);
         out.setDataPoint((cy - 1) * out.dim().width() + (cx - 1), ipolated);
      }
   }

   return out;
}
