/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/04/02                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "MandelbrotCPUMultiStrategy.h"

REGISTER_CALCULATION_STRATEGY(Mandelbrot::Core::MandelbrotCPUMultiStrategy);

Mandelbrot::Core::ComplexDataSet Mandelbrot::Core::MandelbrotCPUMultiStrategy::calculate(const ComplexRect& rect, precision_type pDistance, const ComplexPoint& /*offset*/) const
{
   ComplexDataSet dataSet(rect, pDistance);

#pragma omp parallel for
   for (long long n = 0; n < dataSet.size(); n++)
   {
	   const auto x = n % dataSet.dim().width();
	   const auto y = n / dataSet.dim().width();

      ComplexPoint c(rect.left() + x * pDistance, rect.top() - y * pDistance);
      ComplexPoint z(0, 0);

      iteration_type iterations = 0;
      for (; iterations < getSettings().MaxIterations; iterations++)
      {
         iterations = getSettings().SmoothingAlgorithm->smoothUpdate(n, iterations, getSettings().MaxIterations);
         z = z * z + c;
         if (std::abs(z) > 2)
         {
            break;
         }
      }

      dataSet.setDataPoint(n, getSettings().SmoothingAlgorithm->smoothEnd(c, iterations, getSettings().MaxIterations));
   }

   return dataSet;
}

QString Mandelbrot::Core::MandelbrotCPUMultiStrategy::getName() const
{
   return "Mandelbrot CPU Strategy (multi-threaded)";
}