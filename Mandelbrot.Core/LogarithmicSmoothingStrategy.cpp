/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/21                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/


#include "stdafx.h"
#include "LogarithmicSmoothingStrategy.h"
using namespace Mandelbrot::Core;

// source:
// http://www.iquilezles.org/www/articles/mset_smooth/mset_smooth.htm
SmoothingStrategy::itype LogarithmicSmoothingStrategy::smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const
{
   /*itype dot = endPoint.real() * endPoint.real() + endPoint.imag() * endPoint.imag();
   itype sub = log2(log2(dot));
   itype val = iterations - sub + itype(4);*/

   return iterations;
}
