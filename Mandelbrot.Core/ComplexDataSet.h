/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/28                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexRect.h"

namespace Mandelbrot::Core
{
   /// @brief                 stores the generated data for the complex points (the amounts of iterations it took for each point)
   class MANDELBROTCORE_EXPORT ComplexDataSet
   {
   public: /*typedefs*/

      using ptype = precision_type;
      using itype = iteration_type;

   public: /*ctors*/

      /// @brief              initializes the ComplexDataSet
      /// @param rect         the rect this ComplexDataSet holds the data for (needed for index calculations) (can be changed through setRect(const ComplexRect&))
      /// @param pDistance    the distance between two points (the resolution factor) (can be changed through setPointDistance(ptype pDistance))
      ComplexDataSet(const ComplexRect& rect, ptype pDistance);

      /// @brief              initializes a default ComplexDataSet
      /// @note               for correct usage you must use setRect(const ComplexRect&) and setPointDistance(ptype) next
      ComplexDataSet() = default;

   public: /*methods*/

      /// @brief              set the rect needed for index calculations
      /// @param rect         the rect all data points will be inside of
      void setRect(const ComplexRect& rect);
     
      /// @brief              get the rect this data set holds the data for
      /// @returns            the rect
      const ComplexRect& rect() const;

      /// @brief              set the distance between two points
      /// @param pDistance    the distance between two points
      void setPointDistance(ptype pDistance);

      /// @brief              get the distance between two points that was set
      /// @returns            the distance between points
      ptype pointDistance() const;

      /// @brief              get the max number of data points this object can hold
      /// @returns            the max number of data points this object can hold
      std::size_t size() const;

      /// @brief              get the dimensions of the translated rect map (x / y coordinates)
      /// @returns            the translated dimensions
      const QSize& dim() const;

      /// @brief              get the actual data vector
      /// @returns            the data vector
      const std::vector<itype>& data() const;

      /// @brief              get the actual data vector
      /// @returns            the data vector
      std::vector<itype>& data();

      /// @brief              get the actual data vector as map for ComplexPoint data
      /// @returns            the data map
      std::map<ComplexPoint, itype> dataMap() const;
      
      /// @brief              set the amount of iterations the calculation took for the given point
      /// @param point        the point to set this information for
      /// @param iterations   the amount of iterations the calculation took for the given point
      void setDataPoint(const ComplexPoint& point, itype iterations);

      /// @brief              set the amount of iterations the calculation took for the given index
      /// @param index        the index to set this information for
      /// @param iterations   the amount of iterations the calculation took for the given point
      void setDataPoint(std::size_t index, itype iterations);

      /// @brief              get the amount of iterations the calculation took for the given point
      /// @param point        the point to get the information from
      /// @returns            the amount of iterations
      itype getDataPoint(const ComplexPoint& point) const;

      /// @brief              get the amount of iterations the calculation took for the given index
      /// @param point        the index to get the information from
      /// @returns            the amount of iterations
      itype getDataPoint(std::size_t index) const;

   public: /*operators*/

      bool operator==(const ComplexDataSet& o) const noexcept;
      bool operator!=(const ComplexDataSet& o) const noexcept;

   private: /*methods*/

      /// @brief              calculates the size this object can hold by m_rect and m_pDistance
      /// @note               stores the result in m_size
      void calculateSize();

      /// @brief              calculates the index for this point
      /// @note               the rect and the point distance must be set
      /// @param point        the point to get the index from
      /// @returns            the actual index
      std::size_t calculateIndex(const ComplexPoint& point) const;

   private: /*members*/

      /// @brief              the rect this class needs to calculate indices correctly for the positions
      ComplexRect m_rect = {{0,0},{0,0}};

      /// @brief              the distance between two points
      ptype m_pDistance = {0};

      /// @brief              the amount of data points this object can hold
      std::size_t m_size = {0};

      /// @brief              holds the array bounds
      QSize m_dimensions = {0,0};

      /// @brief              holds the actual data points (the amount of iterations the calculation took)
      std::vector<itype> m_data = {};
   };
}
