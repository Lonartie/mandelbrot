/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexPoint.h"

namespace Mandelbrot::Core
{
   /// @brief                 this class provides rect information about the complex plane
   class MANDELBROTCORE_EXPORT ComplexRect
   {
   public: /*typedefs*/

      using type = precision_type;

   public: /*ctors*/

      /// @brief              inits a complex rect with the \a upperLeft point and the \a lowerRight point
      ComplexRect(const ComplexPoint& upperLeft, const ComplexPoint& lowerRight);

      ComplexRect() = default;
      ComplexRect(const ComplexRect&) = default;
      ComplexRect(ComplexRect&&) = default;
      ~ComplexRect() = default;

   public: /*methods*/

      /// @brief              set the upper left complex point of this rect
      /// @param upperLeft    the upper left complex point
      /// @returns            itself (chaining)
      ComplexRect& setUpperLeft(const ComplexPoint& upperLeft);

      /// @brief              get the upper left complex point of this rect
      /// @returns            the upper left complex point
      const ComplexPoint& getUpperLeft() const;

      /// @brief              sets the lower right complex point of this rect
      /// @param upperLeft    the lower right complex point
      /// @returns            itself (chaining)
      ComplexRect& setLowerRight(const ComplexPoint& lowerRight);

      /// @brief              get the lower right complex point of this rect
      /// @returns            the lower right complex point
      const ComplexPoint& getLowerRight() const;

      /// @brief              calculates the width of the rect
      /// @returns            the width of the rect
      type calculateWidth() const;

      /// @brief              calculates the height of the rect
      /// @returns            the height of the rect
      type calculateHeight() const;

      /// @brief              get the top value (imaginary part) of the rect
      type top() const;

      /// @brief              get the right value (real part) of the rect
      type right() const;

      /// @brief              get the left value (real part) of the rect
      type left() const;

      /// @brief              get the bottom value (imaginary part) of the rect
      type bottom() const;

   public: /*operators*/

      /// @brief              set this data to equals the other objects data
      ComplexRect& operator=(const ComplexRect& o);
      bool operator==(const ComplexRect& o) const noexcept;
      bool operator!=(const ComplexRect& o) const noexcept;

   private: /*members*/

      ComplexPoint m_upperLeft;
      ComplexPoint m_lowerRight;

   };
}