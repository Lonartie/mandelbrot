/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "NoSmoothingStrategy.h"
using namespace Mandelbrot::Core;

SmoothingStrategy::itype NoSmoothingStrategy::smoothUpdate(const ComplexPoint& currentPoint, itype iterations, std::size_t maxIterations) const
{
   return iterations;
}

SmoothingStrategy::itype NoSmoothingStrategy::smoothEnd(const ComplexPoint& endPoint, itype iterations, std::size_t maxIterations) const
{
   return iterations;
}
