/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "CalculationStrategy.h"
#include "ComplexPlaneMapper.h"
#include "ComplexStorage.h"
#include "InterpolationStrategy.h"

namespace Mandelbrot::Core
{
   /// @brief              the main interface to create complex planes and fill it with data (through calculation strategies)
   class MANDELBROTCORE_EXPORT ComplexPlane: public QObject
   {
      Q_OBJECT;

   public: /*ctors*/

      ComplexPlane(const ComplexPlaneMapper& mapper);
      ComplexPlane(const CalculationStrategy& strategy, const ComplexPlaneMapper& mapper);

      ComplexPlane() = delete;
      ComplexPlane(const ComplexPlane&) = delete;
      ComplexPlane(ComplexPlane&&) = delete;
      ~ComplexPlane() = default;

   public: /*methods*/

      const CalculationStrategy& calculationStrategy() const;

   signals:

      /// @brief           the calculations are done
      /// @param data      the calculated data set
      void calculationDone(const ComplexDataSet& data);

   public slots:

      /// @brief           calculate all values on the view
      /// @note            this task may take some time... consider calling it in a thread!
      /// @note            emits calculationDone when done
      /// @param mousePos  the current mouse position relative to the viewports upper left point
      void calculate(const QPoint& mousePos = {-1,-1});

      /// @brief           set the calculation strategy
      /// @param strategy  the new calculation strategy
      void setCalculationStrategy(const CalculationStrategy& strategy);

      /// @brief           set the plane mapper
      /// @param mapper    the new plane mapper
      void setComplexPlaneMapper(const ComplexPlaneMapper& mapper);

      /// @brief           set the interpolation strategy
      /// @param interpol  the interpolation strategy
      void setInterpolationStrategy(const InterpolationStrategy& interpol);

      /// @brief           set whether or not to use the interpolation strategy
      /// @param enabled   whether or not to use the interpolation strategy
      void enableInterpolation(bool enabled);

      /// @brief           whether or not to use the interpolation strategy
      /// @returns         whether or not to use the interpolation strategy
      bool isInterpolationEnabled() const;

      /// @brief           set the interpolation level
      /// @note            only values 1 <= level <= 3 are valid
      /// @param level     the interpolation level
      void setInterpolationLevel(int level);

      /// @brief           get the interpolation level
      /// @returns         the interpolation level
      int interpolationLevel() const;

   private: /*methods*/

      ComplexDataSet interpolateValues(const ComplexDataSet& set) const;

   private: /*members*/

      const CalculationStrategy* m_strategy{};
      const ComplexPlaneMapper* m_mapper;
      const InterpolationStrategy* m_interpolation = nullptr;
      bool m_interpolEnabled = false;
      int m_interpolationLevel = 1;
      ComplexStorage m_dataStorage;
   };
}
