/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

namespace Mandelbrot::Core
{
   /// @brief        the precision type all calculations should be based on
   /// @note         use this whenever you calculate something with fractal data
   using precision_type = float;

   /// @brief        the precision type for counting the iterations in a mandelbrot / julia calculation
   /// @note         use this whenever you count the iterations in a mandelbrot / julia calculation
   using iteration_type = float;
}

