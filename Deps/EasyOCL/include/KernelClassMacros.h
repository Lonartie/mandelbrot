//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "CommonMacros.h"

using OCL_STRING_TYPE = QString;

#define KERNEL_CLASS_H(CLASS) \
public: \
OCL_STRING_TYPE __class_name() { return STRING(CLASS); } \
static std::map<std::size_t, OCL_STRING_TYPE> __class_pointers; \
static std::map<OCL_STRING_TYPE, OCL_STRING_TYPE> __class_functions; \
static std::map<OCL_STRING_TYPE, std::map<EasyOCL::OCLDevice, EasyOCL::OCLProgram>> __class_programs; \
static std::map<OCL_STRING_TYPE, std::vector<OCL_STRING_TYPE>> __function_includes; \
static std::map<OCL_STRING_TYPE, std::vector<std::function<QString()>>> __include_functions; \
static std::map<OCL_STRING_TYPE, OCL_STRING_TYPE> __function_locations; \
static std::map<OCL_STRING_TYPE, std::vector<OCL_STRING_TYPE>> __macros; \
template<typename ...ARGS> \
static QString getKernelCode(EasyOCL::ExecutionResult(*fn)(ARGS...)) { return __class_functions[__class_pointers[FUNCTION_HASH(fn)]]; } \
static const EasyOCL::OCLProgram& getProgram(const QString& name, const EasyOCL::OCLDevice& device); \
template<typename ...ARGS> \
static const EasyOCL::OCLProgram& getProgram(EasyOCL::ExecutionResult(*fn_pointer)(ARGS...), const EasyOCL::OCLDevice& device) \
{ \
	auto _hash = FUNCTION_HASH(fn_pointer); \
	auto& fn = CLASS::__class_pointers[_hash]; \
	return getProgram(fn, device); \
} \
template<typename ...ARGS> static bool compile(EasyOCL::ExecutionResult(*fn_pointer)(ARGS...), const EasyOCL::OCLDevice& device) { return EasyOCL::compile<CLASS>(fn_pointer, device); } \
template<typename ...ARGS> static bool compile(EasyOCL::ExecutionResult(*fn_pointer)(ARGS...)) { return EasyOCL::compile<CLASS>(fn_pointer); } \
static bool compile(const QString& name); \
static bool compile(const QString& name, const EasyOCL::OCLDevice& device); \
private:

#define KERNEL_CLASS_CPP_BEGIN(CLASS) \
std::map<std::size_t, OCL_STRING_TYPE> CLASS::__class_pointers; \
std::map<OCL_STRING_TYPE, OCL_STRING_TYPE> CLASS::__class_functions; \
std::map<OCL_STRING_TYPE, std::map<EasyOCL::OCLDevice, EasyOCL::OCLProgram>> CLASS::__class_programs; \
std::map<OCL_STRING_TYPE, std::vector<OCL_STRING_TYPE>> CLASS::__function_includes; \
std::map<OCL_STRING_TYPE, std::vector<std::function<QString()>>> CLASS::__include_functions; \
std::map<OCL_STRING_TYPE, OCL_STRING_TYPE> CLASS::__function_locations; \
std::map<OCL_STRING_TYPE, std::vector<OCL_STRING_TYPE>> CLASS::__macros; \
bool CLASS::compile(const QString& name)															\
{																												\
	return EasyOCL::compile<CLASS>(name);															\
}																												\
bool CLASS::compile(const QString& name, const EasyOCL::OCLDevice& device)				\
{																												\
	return EasyOCL::compile<CLASS>(name, device);												\
} 																												\
const EasyOCL::OCLProgram& CLASS::getProgram(const QString& name, const EasyOCL::OCLDevice& device) \
{ \
	return CLASS::__class_programs[name][device]; \
} \
__pragma(warning(push)); \
__pragma(warning(disable: 4003));
__pragma(warning(disable: 4002));

#define KERNEL_CLASS_CPP_END() \
__pragma(warning(pop))

#define KERNEL_FUNCTION_H(FN_NAME, ARGS) \
static void CAT(__,FN_NAME)( ARGUMENT_HEADER ARGS ) ; \
static EasyOCL::ExecutionResult FN_NAME(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER ARGS); \
static std::thread CAT(FN_NAME, Async)(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER ARGS, std::function<void()> callback = nullptr); \
static std::thread CAT(FN_NAME, Async)(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER ARGS, std::function<void(EasyOCL::ExecutionResult)> callback);

#define KERNEL_CLASS_DEFINE_HEADER(CLASS, FN_NAME, ...) \
static OCL_STRING_TYPE CAT(CAT(CLASS,FN_NAME),Header) = STRING(kernel void FN_NAME(__VA_ARGS__)) + QString("\n");

#define KERNEL_CLASS_DEFINE_BODY(CLASS, FN_NAME, FN) \
static OCL_STRING_TYPE CAT(CAT(CLASS,FN_NAME),Body) = STRING(FN)

#define KERNEL_CLASS_ADD_FUNCTION(CLASS, FN_NAME) \
static bool CAT(CAT(CLASS,FN_NAME),Added) = EasyOCL::__add_class_function<CLASS>(STRING(FN_NAME), CAT(CAT(CLASS,FN_NAME),Header) + CLASS##FN_NAME##Body); \
static bool CAT(CAT(CLASS,FN_NAME),PAdded) = [&](){ auto fn = &CLASS::FN_NAME; CLASS::__class_pointers.emplace(FUNCTION_HASH(fn), STRING(FN_NAME)); return true; }(); \
static bool CAT(CAT(CLASS,FN_NAME),LAdded) = [&](){ CLASS::__function_locations.emplace(STRING(FN_NAME), OCL_CURRENT_LOCATION ); return true; }();

#define KERNEL_FUNCTION_HEADER(CLASS, FN_NAME, ...) \
EasyOCL::ExecutionResult CLASS::FN_NAME(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER(__VA_ARGS__)) \
{ \
	return EasyOCL::execute<CLASS>(device, iterations, STRING(FN_NAME), ARGUMENTS(__VA_ARGS__)); \
} \
std::thread CLASS::CAT(FN_NAME, Async)(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER(__VA_ARGS__), std::function<void()> callback) \
{ \
	std::thread _worker([=]() { CLASS::FN_NAME(device, iterations, ARGUMENTS(__VA_ARGS__)); if(callback) callback(); }); \
	return _worker; \
} \
std::thread CLASS::CAT(FN_NAME, Async)(const EasyOCL::OCLDevice& device, std::size_t iterations, CL_FUNC_ARGUMENT_HEADER(__VA_ARGS__), std::function<void(EasyOCL::ExecutionResult)> callback) \
{ \
	std::thread _worker([=]() { if(callback) callback(CLASS::FN_NAME(device, iterations, ARGUMENTS(__VA_ARGS__))); else CLASS::FN_NAME(device, iterations, ARGUMENTS(__VA_ARGS__)); }); \
	return _worker; \
} \
KERNEL_CLASS_DEFINE_HEADER(CLASS, FN_NAME, ARGUMENT_HEADER(__VA_ARGS__)); \
void CLASS::CAT(__,FN_NAME)(ARGUMENT_HEADER(__VA_ARGS__))

#define KERNEL_FUNCTION_BODY(FN, CLASS, FN_NAME) \
FN \
KERNEL_CLASS_DEFINE_BODY(CLASS, FN_NAME, FN); \
KERNEL_CLASS_ADD_FUNCTION(CLASS, FN_NAME); 

#define INCLUDE_CLASS(CLASS, FN_NAME, INC_CLASS) \
static bool CAT(CAT(CAT(CLASS,FN_NAME),INC_CLASS),__included) = [&]() \
{ \
	auto function_exist = std::find_if(CLASS::__include_functions.begin(), CLASS::__include_functions.end(), [&](auto& kvp) { return kvp.first == STRING(FN_NAME); }) != CLASS::__include_functions.end(); \
	if (function_exist) \
		CLASS::__include_functions[STRING(FN_NAME)].push_back(&INC_CLASS::getAll); \
	else \
	{ \
		std::vector<std::function<QString()>> tmp = {&INC_CLASS::getAll}; \
		CLASS::__include_functions.emplace(STRING(FN_NAME), tmp); \
	} \
	return true; \
}();

#define KERNEL_FUNCTION_INCLUDE_CLASSES(CLASS, FN_NAME, INCLUDING_CLASSES) \
CL_FOREACH_WITH_ARGS(INCLUDE_CLASS, INCLUDE_CLASS, (CLASS, FN_NAME), UNPACK INCLUDING_CLASSES)

#define KERNEL_FUNCTION_CPP(CLASS, FN_NAME, ARGS, FN, ...) \
KERNEL_FUNCTION_HEADER(CLASS, FN_NAME, UNPACK ARGS ) \
KERNEL_FUNCTION_BODY(FN, CLASS, FN_NAME) \
KERNEL_FUNCTION_INCLUDE_CLASSES(CLASS, FN_NAME, PACK(__VA_ARGS__)) 

#define KERNEL_FUNCTION_USE_CLASSES(CLASS, FN_NAME, ...) \
KERNEL_FUNCTION_INCLUDE_CLASSES(CLASS, FN_NAME, PACK(__VA_ARGS__))

#define KERNEL_REGISTER_MACRO_CONSTANT(CLASS, FN_NAME, MAC_NAME, MAC_VALUE, TYPE) \
const TYPE MAC_NAME = MAC_VALUE; \
static const bool CAT(CAT(CAT(CLASS, FN_NAME),MAC_NAME),Added) = []() \
{																																						\
	auto& map = CLASS::__macros;																												\
	if (std::find_if(map.begin(), map.end(), [](auto& kvp) {return kvp.first==STRING(FN_NAME);}) == map.end())		\
		map.emplace(STRING(FN_NAME), std::vector<OCL_STRING_TYPE> { STRING(MAC_NAME=MAC_VALUE) });					\
	else																																				\
		map[STRING(FN_NAME)].push_back(STRING(MAC_NAME=MAC_VALUE));																\
	return true; 																																	\
}();

#define TO_PARAM_COMMA(TYPE, NAME) TYPE NAME ,
#define TO_PARAM(TYPE, NAME) TYPE NAME

#define KERNEL_REGISTER_MACRO_FUNCTION(CLASS, FN_NAME, MAC_NAME, MAC_VALUE, VAR_NAMES) \
float MAC_NAME ( CL_FOREACH_WITH_ARGS(TO_PARAM, TO_PARAM_COMMA, (float), UNPACK VAR_NAMES) ) { return ( MAC_VALUE ); }; \
static const bool CAT(CAT(CAT(CLASS, FN_NAME),MAC_NAME),Added) = []() \
{																																						\
	auto& map = CLASS::__macros;																												\
	if (std::find_if(map.begin(), map.end(), [](auto& kvp) {return kvp.first==STRING(FN_NAME);}) == map.end())		\
		map.emplace(STRING(FN_NAME), std::vector<OCL_STRING_TYPE> { STRING(MAC_NAME##VAR_NAMES##=MAC_VALUE) });	\
	else																																				\
		map[STRING(FN_NAME)].push_back(STRING(MAC_NAME##VAR_NAMES##=MAC_VALUE));											\
	return true; 																																	\
}();

#define EXTERN_MACRO_CONSTANT(NAME, TYPE) \
extern const TYPE NAME;

#define EXTERN_MACRO_FUNCTION(NAME, VAR_NAMES) \
float NAME ( CL_FOREACH_WITH_ARGS(TO_PARAM, TO_PARAM_COMMA, (float), UNPACK VAR_NAMES) );