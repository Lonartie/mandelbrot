//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"

namespace EasyOCL
{

   enum DeviceType { CPU, GPU, Others };
   enum Vendor { Intel, NVidia, AMD, Unknown };

	class EASYOCL_EXPORT OCLDevice
	{
	public:

		/// @brief if GPU doesn't exist, returns default device
		static OCLDevice GPU();
		/// @brief if CPU doesn't exist, returns default device
		static OCLDevice CPU();

		static std::vector<OCLDevice> getDevices();

		OCLDevice() = default;
      OCLDevice(void* data);
		OCLDevice(OCLDevice&& o);
		OCLDevice(const OCLDevice& o);
		~OCLDevice();

		const void* data() const;

		bool operator==(const OCLDevice& o) const noexcept;
		bool operator!=(const OCLDevice& o) const noexcept;
		bool operator<(const OCLDevice& o) const noexcept;

		OCLDevice& operator=(const OCLDevice& o);
		OCLDevice& operator=(OCLDevice&& o);

      QString getName();
      QString getName() const;
		DeviceType getDeviceType() const;
		Vendor getVendor() const;

		/// @note do no rely on the result!
      std::size_t getClockFrequency() const;
      /// @note do no rely on the result!
      std::size_t getAvailableMemory() const;
      /// @note do no rely on the result!
      std::size_t getWorkGroupSize() const;
      /// @note do no rely on the result!
      std::size_t getComputeUnits() const;
      /// @note do no rely on the result!
		QString getDriverVersion() const;

	private:

		static void* getGPU();
		static void* getCPU();

		QString m_name;
		void* m_data = nullptr;
	};
}