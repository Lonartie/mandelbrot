//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(EASYOCL_LIB)
#  define EASYOCL_EXPORT Q_DECL_EXPORT
# else
#  define EASYOCL_EXPORT Q_DECL_IMPORT
# endif
#else
# define EASYOCL_EXPORT
#endif
