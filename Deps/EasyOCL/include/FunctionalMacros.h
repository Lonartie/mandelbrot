//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "CommonMacros.h"

#define OCL_CURRENT_LOCATION QString("File: %1, Function: %2, Line: %3").arg(QFileInfo(__FILE__).baseName()).arg(__FUNCTION__ "").arg(__LINE__)
