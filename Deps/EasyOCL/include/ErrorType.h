//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

namespace EasyOCL
{
   enum ErrorType
   {
      BUILD_ERROR,
      RUNTIME_ERROR,
      BUFFER_CREATION_ERROR,
      WRITE_BUFFER_ERROR,
		EXECUTION_ERROR,

      OTHER_ERROR,
      NO_CL_ERROR
   };
}