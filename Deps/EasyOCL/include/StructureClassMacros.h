//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "CommonMacros.h"

#define STRUCTURE_CLASS_H(CLASS)                            \
public:                                                     \
static std::vector<QString> __fields;                       \
static inline QString getAll()                              \
{                                                           \
   QString __this;                                          \
   for (const auto& field : __fields)                       \
   __this += field + "\n";                                  \
   __this = STRING(typedef struct CLASS { ) + __this + STRING( } CLASS; );\
	return QString("/* included from %1:*/\n%2")             \
   .arg(STRING(CLASS))                                      \
   .arg(__this);                                            \
}

#define STRUCTURE_FIELD_H(TYPE, NAME)                       \
TYPE NAME;

#define STRUCTURE_FIELD_CPP(CLASS, TYPE, NAME)              \
static const bool CAT(CAT(CLASS,NAME),ADDED) = []()         \
{ CLASS::__fields.push_back(STRING(TYPE NAME;)); return true; }();

#define STRUCTURE_CLASS_CPP_BEGIN(CLASS)                    \
__pragma(warning(push))                                     \
__pragma(warning(disable: 4002))                            \
__pragma(warning(disable: 4003))                            \
std::vector<QString> CLASS::__fields;

#define STRUCTURE_CLASS_CPP_END()                           \
__pragma(warning(pop))

