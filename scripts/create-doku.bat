@echo off

rem go to root dir
cd ..

rem delete old doxygen folder
rem rmdir Doxygen /Q /S

rem run doxygen
doxygen doxyfile.doxy

rem copy modified build script
copy "%cd%\scripts\make.bat" "%cd%\Doxygen\latex\make.bat" /Y

rem create pdf from latex
call "%cd%\Doxygen\latex\make.bat"

rem copy output pdf
copy "%cd%\Doxygen\latex\refman.pdf" "%cd%\Doxygen\Mandelbrot-Dokumentation.pdf" /Y
