/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

// Qt
#include <QWidget>

// Form file
#include "ui_Dashboard.h"

namespace Mandelbrot::GUI
{
	/// @brief Dashboard widget displaying the mandelbrot and julia view.
	class Dashboard : public QWidget
	{
		Q_OBJECT

	public:
		Dashboard(QWidget* parent = Q_NULLPTR);
		~Dashboard();

	private:
		Ui::Dashboard m_ui;
	};
}