/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.GUI
#include "Dashboard.h"

using namespace Mandelbrot::GUI;
int main(int argc, char* argv[])
{
   QApplication a(argc, argv);

   Dashboard dashboard;
   dashboard.show();

   return a.exec();
}
