/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

#include "stdafx.h"
#include "FractalView.h"

// Mandelbrot.Core
#include "Mandelbrot.Core/MandelbrotCPUStrategy.h"
#include "Mandelbrot.Core/LogarithmicSmoothingStrategy.h"

namespace Mandelbrot::GUI
{
	/// @brief View for the mandelbrot fractal.
	class MandelbrotView : public FractalView
	{
		Q_OBJECT

	public:
		MandelbrotView(QWidget* parent = Q_NULLPTR);
		~MandelbrotView();

	private:
		Core::LogarithmicSmoothingStrategy m_smoothingStrategy;
		Core::CalculationSettings m_settings;
		Core::MandelbrotCPUStrategy m_strategy;
	};
}