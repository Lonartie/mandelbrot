/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/14                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexPlaneItem.h"
#include "BlackWhiteMapper.h"
#include "Mandelbrot.Core/NearestNeighbourStrategy.h"
#include "Mandelbrot.Core/BilinearInterpolationStrategy.h"
using namespace Mandelbrot::GUI;
using namespace Mandelbrot::Core;

ComplexPlaneItem::ComplexPlaneItem(ComplexPlaneGraphicsView* view, QGraphicsItem* parent /*= Q_NULLPTR*/)
   : QGraphicsPixmapItem(parent)
   , m_view(view)
   , m_plane(m_mapper)
{
   m_colorMapper = new BlackWhiteMapper();
   m_interpolation = new BilinearInterpolationStrategy();

   connect(&m_plane, &ComplexPlane::calculationDone, this, &ComplexPlaneItem::recreateImage);

   m_mapper.setVisibleRect(ComplexRect({-2, 2}, {2, -2}), m_view->viewport()->size());
   m_plane.setInterpolationLevel(1);
   //m_plane.enableInterpolation(true);
   m_plane.setInterpolationStrategy(*m_interpolation);
}

void ComplexPlaneItem::requestCalculation()
{
   updateMapper();
   m_plane.calculate(m_clickPoint);
}

void ComplexPlaneItem::recreateImage(const ComplexDataSet& data)
{
	const auto dim = data.dim();
   QImage image(dim, QImage::Format_RGB888);

#pragma omp parallel for
   for (long long index = 0; index < data.size(); index++)
   {
      const auto value = data.data()[index];
      const auto x = index % dim.width();
      const auto y = index / dim.width();

      const auto color = m_colorMapper->map(value, 
                                            m_plane.calculationStrategy().getSettings().MaxIterations,
                                            m_mapper.translate(QPointF(x, y)));

      image.setPixelColor(x, y, color);
   }

   setPixmap(QPixmap::fromImage(std::move(image)));
   scene()->setSceneRect(QRect(QPoint(0, 0), dim));
   m_view->resetMatrix();
}

void ComplexPlaneItem::setCalculationStrategy(const CalculationStrategy& strategy)
{
   m_plane.setCalculationStrategy(strategy);
}

void ComplexPlaneItem::updateMapper()
{
   const auto scene_rect = QRectF(QPointF(0, 0), scene()->sceneRect().size());
   const auto view_rect = m_view->calculateVisibleRect();

   const auto newVisibleRect = ComplexPlaneMapper::calculateVisibleRect(scene_rect, view_rect, m_mapper.getVisibleRect());

   m_mapper.setVisibleRect(newVisibleRect, m_view->viewport()->size());
}

const ComplexPlaneMapper& ComplexPlaneItem::getMapper() const
{
	return m_mapper;
}

void ComplexPlaneItem::setClickPoint(const QPoint& point)
{
	m_clickPoint = point;
}

ComplexPlaneItem::~ComplexPlaneItem()
{
   if (m_colorMapper) 
      delete m_colorMapper;
}

