/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Dashboard.h"

namespace Mandelbrot::GUI
{
	Dashboard::Dashboard(QWidget* parent)
		: QWidget(parent)
	{
		m_ui.setupUi(this);

		connect(m_ui.mandelbrot_view, &FractalView::mouseClicked, [=]() {m_ui.julia_view->setClickPoint(QCursor::pos()); });
	}

	Dashboard::~Dashboard()
	{}
}