/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "OverlayMiniMapItem.h"
using namespace Mandelbrot::GUI;

OverlayMiniMapItem::OverlayMiniMapItem(QGraphicsView* view, QGraphicsItem* parent /*= Q_NULLPTR*/)
    : QGraphicsOverlayItem(view, parent)
{
   m_mapItem = m_scene.addPixmap(QPixmap(boundingRect().size().toSize()));
   m_selectionItem = m_scene.addRect(boundingRect(), QPen(Qt::darkBlue), QBrush(QColor(0, 0, 128, 128)));
}

QRectF OverlayMiniMapItem::boundingRect() const
{
	const QSize minimap_size(100, 100);
	const QPoint minimap_pos(-10, 10);

   return QRectF(minimap_pos, minimap_size);
}

void OverlayMiniMapItem::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget /*= nullptr*/)
{
	const auto rect = boundingRect();

   // Draw the map itself
   QPixmap map(scene()->sceneRect().size().toSize());
   map.fill(Qt::white);
   QPainter p(&map);

   // render without this item
   scene()->removeItem(this);
   scene()->render(&p);
   scene()->addItem(this);

   // add to virtual scene
   m_mapItem->setPixmap(map);

   // Draw the drag rect to move through the view
	const auto topLeft = view()->mapToScene(0, 0);
	const auto size = view()->viewport()->geometry().size();
	const auto bottomRight = view()->mapToScene(QPoint(size.width(), size.height()));
	const QRectF visibleRect(topLeft, bottomRight);
   m_selectionItem->setRect(visibleRect);

   // Draw output
   m_scene.setSceneRect(scene()->sceneRect());
	const auto drawRect = addPercentageOffset(scene()->sceneRect(), .1);
	const auto drawSize = drawRect.size().toSize();
	const auto sceneRect = scene()->sceneRect();
   //sceneRect.setSize(drawRect.size().scaled(sceneRect.size(), Qt::KeepAspectRatio));
   QPixmap out(drawSize);
   out.fill(Qt::white);
   QPainter pout(&out);
   m_scene.render(&pout, QRectF(), sceneRect);

	const auto outSize = drawRect.size().scaled(rect.size(), Qt::KeepAspectRatio);

	const auto w2 = outSize.width();
	const auto h2 = outSize.height();
	const auto x2 = rect.left() + (rect.width() - w2) / 2;
	const auto y2 = rect.top() + (rect.height() - h2) / 2;

	const auto outRect = QRect(x2, y2, w2, h2);

   // prefill white
   painter->fillRect(rect.toRect(), Qt::white);

   // draw minimap
   painter->drawPixmap(outRect, out);

   // draw rect around map
   painter->setPen(QPen(Qt::black, 3));
   painter->drawRect(rect.toRect());
}

QRectF OverlayMiniMapItem::addPercentageOffset(QRectF rect, double percentage)
{
	const auto left = rect.left();
	const auto top = rect.top();
	const auto width = rect.width();
	const auto height = rect.height();

	const auto nwidth = width * (1.0 + percentage);
	const auto nheight = height * (1.0 + percentage);

	const auto xgain = nwidth - width;
	const auto ygain = nheight - height;

	const auto nleft = left - xgain / 2.0;
	const auto ntop = top - ygain / 2.0;

   return QRectF(nleft, ntop, nwidth, nheight);
}

QRectF OverlayMiniMapItem::addPixelOffset(QRectF rect, int offset)
{
	const auto left = rect.left();
	const auto top = rect.top();
	const auto width = rect.width();
	const auto height = rect.height();

	const auto nwidth = width + 2 * offset;
	const auto nheight = height + 2 * offset;

	const auto nleft = left - offset;
	const auto ntop = top - offset;

   return QRectF(nleft, ntop, nwidth, nheight);
}
