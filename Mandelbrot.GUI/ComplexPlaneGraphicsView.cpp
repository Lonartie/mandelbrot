/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ComplexPlaneGraphicsView.h"
using namespace Mandelbrot::GUI;

ComplexPlaneGraphicsView::ComplexPlaneGraphicsView(QWidget* parent /*= Q_NULLPTR*/)
   : QGraphicsView(parent)
   , m_scene(this)
{
   setScene(&m_scene);
   setDragMode(ScrollHandDrag);
   setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   setResizeAnchor(AnchorViewCenter);
}

void ComplexPlaneGraphicsView::wheelEvent(QWheelEvent* event)
{
	const ViewportAnchor previous_anchor = transformationAnchor();
   setTransformationAnchor(AnchorUnderMouse);
   
   if (event->delta() > 0)
   {
      scale(1.1, 1.1);
      m_currentScaling *= 1.1;
   }
   else
   {
      scale(0.9, 0.9);
      m_currentScaling *= 0.9;
   }

   emit planeScaled(m_currentScaling);
   setTransformationAnchor(previous_anchor);
   calculateMovement();
}

void ComplexPlaneGraphicsView::calculateMovement()
{
	const QPointF newMid = mapFromScene(0, 0);
	const QPointF delta = newMid - m_midPoint;
   m_midPoint = newMid;
   emit planeMoved(delta);
}

void ComplexPlaneGraphicsView::resizeEvent(QResizeEvent* event)
{
   QGraphicsView::resizeEvent(event);
   emit viewResized(event->size());
}

void ComplexPlaneGraphicsView::mousePressEvent(QMouseEvent* event)
{
   QGraphicsView::mousePressEvent(event);
   m_dragging = true;
}

void ComplexPlaneGraphicsView::mouseMoveEvent(QMouseEvent* event)
{
   QGraphicsView::mouseMoveEvent(event);
   if (m_dragging) calculateMovement();
}

void ComplexPlaneGraphicsView::mouseReleaseEvent(QMouseEvent* event)
{
   QGraphicsView::mouseReleaseEvent(event);
   m_dragging = false;
}

QRectF ComplexPlaneGraphicsView::calculateVisibleRect() const
{
	const QPointF topLeft = mapToScene(QPoint(0, 0));
	const QPointF bottomRight = mapToScene(QPoint(viewport()->width(), viewport()->height()));
   return QRectF(topLeft, bottomRight);
}
