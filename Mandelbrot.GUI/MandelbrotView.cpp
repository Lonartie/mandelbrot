/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.GUI
#include "MandelbrotView.h"
#include "Mandelbrot.Core/CalculationStrategy.h"
#include "Mandelbrot.Core/CalculationSettings.h"

namespace Mandelbrot::GUI
{
   MandelbrotView::MandelbrotView(QWidget* parent)
      : FractalView(parent)
        , m_settings(64, m_smoothingStrategy)
   {
      //auto dev = EasyOCL::OCLDevice::CPU();

      //if (!Core::MandelbrotOCLStrategy::compile(&Core::MandelbrotOCLStrategy::kernelCalculate, dev))
      //{
      //   auto errStr = Core::MandelbrotOCLStrategy::getProgram(&Core::MandelbrotOCLStrategy::kernelCalculate, dev).error;
      //   // compilation failed
      //   __debugbreak();
      //}

      //if (dev.getDeviceType() != EasyOCL::CPU)
      //{
      //   // no appropriate opencl driver installed
      //   // for testing please install opencl runtime for your cpu
      //   __debugbreak();
      //}

      m_strategy.setSettings(m_settings);
      setCalculationStrategy(m_strategy);
   }

   MandelbrotView::~MandelbrotView()
   {}
}