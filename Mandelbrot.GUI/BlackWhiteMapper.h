/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/14                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ColorMappingStrategy.h"

// Mandelbrot.Core
#include "Mandelbrot.Core/ComplexPoint.h"

namespace Mandelbrot::GUI
{
   class BlackWhiteMapper final : public ColorMappingStrategy
   {
   public:

	   QColor map(float iterations, float maxIterations, Core::ComplexPoint position) const override;

   };
}