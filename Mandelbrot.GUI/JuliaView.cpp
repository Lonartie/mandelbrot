/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "JuliaView.h"

using namespace Mandelbrot::GUI;

JuliaView::JuliaView(QWidget* parent)
	: FractalView(parent)
	, m_settings(64, m_smoothingStrategy)
{
	m_strategy.setSettings(m_settings);
	setCalculationStrategy(m_strategy);
}

JuliaView::~JuliaView()
{}
