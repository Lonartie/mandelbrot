/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/14                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

// Mandelbrot.Core
#include "Mandelbrot.Core/ComplexPoint.h"

namespace Mandelbrot::GUI
{
   class ColorMappingStrategy
   {
   public:
      virtual ~ColorMappingStrategy() = default;
      virtual QColor map(float iterations, float maxIterations, Core::ComplexPoint position) const = 0;
   };
}
