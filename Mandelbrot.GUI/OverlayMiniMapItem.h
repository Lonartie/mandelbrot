/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "QGraphicsOverlayItem.h"

namespace Mandelbrot::GUI
{
   /// @brief                    a QGraphicsOverlayItem displaying a minimap of the current view 
   class OverlayMiniMapItem: public QGraphicsOverlayItem
   {
      Q_OBJECT;

   public: /*ctors*/

      OverlayMiniMapItem(QGraphicsView* view, QGraphicsItem* parent = Q_NULLPTR);
      virtual ~OverlayMiniMapItem() = default;

   protected: /*inherited methods from QGraphicsOverlayItem*/

      /// @copydoc               Mandelbrot::GUI::QGraphicsOverlayItem::boundingRect() const
      QRectF boundingRect() const override;

      /// @copydoc               Mandelbrot::GUI::QGraphicsOverlayItem::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
      void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

   private: /*static methods*/

      /// @brief                 adds a percentage offset to the given rect
      /// @param rect            the rect to add the offset to
      /// @param percentage      the percentage to calculate the offset size
      /// @returns               the new calculated rect
      static QRectF addPercentageOffset(QRectF rect, double percentage);

      /// @brief                 adds an offset to the given rect
      /// @param rect            the rect to add the offset to
      /// @param offset          the offset to add to the rect
      /// @returns               the new calculated rect
      static QRectF addPixelOffset(QRectF rect, int offset);

   private: /*members*/

      /// @brief                 the sub-scene
      QGraphicsScene m_scene;

      /// @brief                 the real minimap graphics item for sub-scene
      QGraphicsPixmapItem* m_mapItem;

      /// @brief                 the selection rect for sub-scene (viewport)
      QGraphicsRectItem* m_selectionItem;
   };
}