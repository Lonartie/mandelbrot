#include "stdafx.h"
#include "BlackWhiteMapper.h"
using namespace Mandelbrot::GUI;
using namespace Mandelbrot::Core;

QColor BlackWhiteMapper::map(float iterations, float maxIterations, ComplexPoint /*position*/) const
{
	const std::size_t value = static_cast<std::size_t>(std::abs(iterations / maxIterations * 255));
   return QColor(value, value, value);
}
