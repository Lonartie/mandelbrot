/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

namespace Mandelbrot::GUI
{
   /// @brief              a custom QGraphicsView for visualizing ComplexPlanes (basically a zoomable and movable view)
   class ComplexPlaneGraphicsView: public QGraphicsView
   {
      Q_OBJECT;

   public: /*ctors*/

      ComplexPlaneGraphicsView(QWidget* parent = Q_NULLPTR);
      virtual ~ComplexPlaneGraphicsView() = default;

   public:
      QRectF calculateVisibleRect() const;

   signals:
      void planeMoved(const QPointF& delta);
      void viewResized(const QSize& newSize);
      void planeScaled(double newScaling);

   protected: /*inherited methods from QGraphicsView*/

      /// @brief           scales the view when scrolling
      void wheelEvent(QWheelEvent* event) override;

      void resizeEvent(QResizeEvent* event) override;

      void mousePressEvent(QMouseEvent* event) override;

      void mouseMoveEvent(QMouseEvent* event) override;

      void mouseReleaseEvent(QMouseEvent* event) override;

   private slots:
      void calculateMovement();

   private: /*members*/

      /// @brief           the predefined scene object
      QGraphicsScene m_scene;

      double m_currentScaling = 1;

      QPointF m_midPoint = {0,0};

      bool m_dragging = false;
   };
}