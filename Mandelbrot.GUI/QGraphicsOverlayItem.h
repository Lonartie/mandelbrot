/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

namespace Mandelbrot::GUI
{
   /// @brief              a QGraphicsItem but displaying dependent on the viewport and not the scene
   class QGraphicsOverlayItem: public QObject, public QGraphicsItem
   {
      Q_OBJECT;
      Q_INTERFACES(QGraphicsItem)

   public: /*ctors*/

      QGraphicsOverlayItem(QGraphicsView* view, QGraphicsItem* parent = Q_NULLPTR);
      virtual ~QGraphicsOverlayItem() = default;

   protected: /*inherited methods from QGraphicsItem*/

      /// @brief           get the bounding rect for this widget
      QRectF boundingRect() const override = 0;

      /// @brief           paint this widget to display it
      void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override = 0;

   protected: /*methods*/

      /// @brief           get the view this object is displayed on
      QGraphicsView* view() const;

      /// @brief           get the scene this object is in
      QGraphicsScene* scene() const;

   private slots:

      /// @brief           adjust the overlay position to match correctly
      void adjustPosition();

      /// @brief           adjust the scene rect to be calculated without this overlay item
      void adjustSceneRect();

   private /*methods*/:

      /// @brief           translates a rect from scene space to viewport space
      QPoint translateRect(const QRectF& rect) const;

   private: /*members*/

      /// @brief           the view this object is displayed on
      QGraphicsView* m_parentView;

      /// @brief           the scene this object is in
      QGraphicsScene* m_parentScene;
   };
}