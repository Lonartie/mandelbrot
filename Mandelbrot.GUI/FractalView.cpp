/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.GUI
#include "FractalView.h"
#include "OverlayMiniMapItem.h"
using namespace Mandelbrot::GUI;
using namespace Mandelbrot::Core;

FractalView::FractalView(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	m_planeItem = new ComplexPlaneItem(m_ui.view);

	m_ui.view->scene()->addItem(new OverlayMiniMapItem(m_ui.view));
	m_ui.view->scene()->addItem(m_planeItem);
}

FractalView::~FractalView()
{
	m_planeItem->deleteLater();
}

void FractalView::setClickPoint(const QPoint& point)
{
	m_planeItem->setClickPoint(point);
	recalculate();
}

void FractalView::setCalculationStrategy(const CalculationStrategy& strategy)
{
	m_strategy = &strategy;
	m_planeItem->setCalculationStrategy(strategy);
}

void FractalView::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Space)
	{
		recalculate();
	}
}

void FractalView::mousePressEvent(QMouseEvent* /*event*/)
{
	emit mouseClicked();
}

void FractalView::recalculate()
{
	auto* worker = new QThread(this);
	m_planeItem->moveToThread(worker);
	connect(worker, &QThread::started, m_planeItem, &ComplexPlaneItem::requestCalculation);
	connect(worker, &QThread::finished, [=]() { m_planeItem->moveToThread(QApplication::instance()->thread()); });
	connect(worker, &QThread::finished, worker, &QObject::deleteLater);
	worker->start();
}

void FractalView::showEvent(QShowEvent* event)
{
	recalculate();
}
