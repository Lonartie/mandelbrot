/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/14                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ComplexPlaneGraphicsView.h"
#include "ColorMappingStrategy.h"

// Mandelbrot.Core
#include "Mandelbrot.Core/ComplexPlane.h"
#include "Mandelbrot.Core/ComplexPlaneMapper.h"
#include "Mandelbrot.Core/CalculationStrategy.h"
#include "Mandelbrot.Core/ComplexDataSet.h"
#include "Mandelbrot.Core/InterpolationStrategy.h"

namespace Mandelbrot::GUI
{
   class ComplexPlaneItem: public QObject, public QGraphicsPixmapItem
   {
      Q_OBJECT;

   public:
      ComplexPlaneItem(ComplexPlaneGraphicsView* view, QGraphicsItem* parent = Q_NULLPTR);
      ~ComplexPlaneItem();

      [[nodiscard]]
   	const Core::ComplexPlaneMapper& getMapper() const;

   	void setClickPoint(const QPoint& point);
   	
   public slots:
      void setCalculationStrategy(const Core::CalculationStrategy& strategy);
      void requestCalculation();

   private slots:
      void recreateImage(const Core::ComplexDataSet& data);
      void updateMapper();

   private:
      ComplexPlaneGraphicsView* m_view = nullptr;
      const ColorMappingStrategy* m_colorMapper = nullptr;
      const Core::InterpolationStrategy* m_interpolation = nullptr;

      Core::ComplexPlaneMapper m_mapper;
      Core::ComplexPlane m_plane;
   	QPoint m_clickPoint;
   };
}