/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

#include "stdafx.h"

// Mandelbrot.Core
#include "Mandelbrot.Core/CalculationStrategy.h"

// Form file
#include "ui_FractalView.h"
#include "ComplexPlaneItem.h"

namespace Mandelbrot::GUI
{
	/// @brief View for displaying a fractal (like the mandelbrot or julia set).
	class FractalView: public QWidget
	{
		Q_OBJECT

	public:
		FractalView(QWidget* parent = Q_NULLPTR);
		~FractalView();

		void setClickPoint(const QPoint& point);
		
	signals:
		void mouseClicked();

	public:
		void showEvent(QShowEvent* event) override;
		void keyPressEvent(QKeyEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;

		void recalculate();
		void setCalculationStrategy(const Core::CalculationStrategy& strategy);

	private:
		const Core::CalculationStrategy* m_strategy = nullptr;
		ComplexPlaneItem* m_planeItem = nullptr;
		Ui::FractalView m_ui{};
	};
}