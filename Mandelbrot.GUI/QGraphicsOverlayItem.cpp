/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/07                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "QGraphicsOverlayItem.h"
using namespace Mandelbrot::GUI;

QGraphicsOverlayItem::QGraphicsOverlayItem(QGraphicsView* view, QGraphicsItem* parent /*= Q_NULLPTR*/)
   : QGraphicsItem(parent)
   , m_parentView(view)
   , m_parentScene(view->scene())
{
   // this item is an overlay item
   setFlag(ItemIgnoresTransformations);
   setFlag(ItemHasNoContents, false);
   setFlag(ItemIsMovable, false);
   setZValue(999);

   // full viewport update to counteract visual artifacts
   m_parentView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

   // every time the scene has changed adjust position
   connect(m_parentScene, &QGraphicsScene::changed, this, &QGraphicsOverlayItem::adjustPosition);
   connect(m_parentScene, &QGraphicsScene::changed, this, &QGraphicsOverlayItem::adjustSceneRect);
}

void QGraphicsOverlayItem::adjustSceneRect()
{
   scene()->removeItem(this);
   const auto srect = scene()->itemsBoundingRect();
   scene()->addItem(this);
   scene()->setSceneRect(srect);
}

void QGraphicsOverlayItem::adjustPosition()
{
	const auto rect = boundingRect();
	const auto adjusted = translateRect(rect);
	const auto translated = m_parentView->mapToScene(adjusted);
   setPos(translated);
   m_parentScene->invalidate();
   m_parentView->update();
}

QGraphicsView* QGraphicsOverlayItem::view() const
{
   return m_parentView;
}

QGraphicsScene* QGraphicsOverlayItem::scene() const
{
   return m_parentScene;
}

QPoint QGraphicsOverlayItem::translateRect(const QRectF& rect) const
{
   QPoint result;
   const auto srect = m_parentView->viewport()->rect();

   if (rect.left() < 0) result.setX(srect.width() + rect.left() + srect.left() - rect.width());
   else result.setX(rect.x());
   if (rect.top() < 0) result.setY(srect.height() + rect.top() + srect.top() - rect.height());
   else result.setY(rect.y());

   return result;
}

