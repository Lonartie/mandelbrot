/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/26                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "FractalView.h"
#include "ConsoleInterop.h"
using namespace Mandelbrot::Console;

FractalView::FractalView()
{
   setBorder(true, '*');
   connect(this, &ConsoleWidget::update, this, &FractalView::drawTitle);
   connect(this, &ConsoleWidget::clicked, this, &FractalView::drawOtherTitle);
}

void FractalView::drawTitle() const
{
   // reset the drawing area
   //fill(getDrawableRect(), ' ');

   // draw text
   setPosition(getDrawableRect().topLeft());
   QString text = getTitle();
   QString pad;
   const auto padding = getDrawableRect().width() - text.count();
   for (int i = 0; i < padding / 2 - 1; i++) pad += " ";
   text = pad + text + pad;
   ConsoleInterop::Instance().setConsoleOutputColor(DisplayColor(ConsoleInterop::Instance().getCurrentBackgroundColor() | Green_Foreground));
   write(text);
   ConsoleInterop::Instance().resetConsoleOutputColor();

   // draw underscore line
   setPosition(getDrawableRect().topLeft() + QPoint(0, 1));
   QString line;
   for (int i = 0; i < getDrawableRect().width(); i++)
      line += "=";
   write(line);

   // reset the cursor position
   resetCursor();
}

void FractalView::drawOtherTitle()
{
   // reset the drawing area
   //fill(getDrawableRect(), ' ');

   // draw text
   setPosition(getDrawableRect().topLeft());
   QString text = getTitle() + " CLICKED: " + QString::number(++m_clickedCounter);
   QString pad;
   const auto padding = getDrawableRect().width() - text.count();
   for (int i = 0; i < padding / 2 - 1; i++) pad += " ";
   text = pad + text + pad;
   ConsoleInterop::Instance().setConsoleOutputColor(DisplayColor(ConsoleInterop::Instance().getCurrentBackgroundColor() | Green_Foreground));
   write(text);
   ConsoleInterop::Instance().resetConsoleOutputColor();

   // draw underscore line
   setPosition(getDrawableRect().topLeft() + QPoint(0, 1));
   QString line;
   for (int i = 0; i < getDrawableRect().width(); i++)
      line += "=";
   write(line);

   // reset the cursor position
   resetCursor();
}
