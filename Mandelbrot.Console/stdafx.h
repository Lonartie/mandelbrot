/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once

// Mandelbrot.Core
#include "Mandelbrot.Core/stdafx.h"

// QtCore
#include <QCoreApplication>
#include <QRect>
#include <QTimer>
#include <QThread>
#include <QMetaType>
