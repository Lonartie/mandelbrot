/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma comment(lib, "User32.lib")

#include "stdafx.h"
#include "ConsoleInterop.h"
#include <windows.h>
#include <iostream>
using namespace Mandelbrot::Console;

namespace
{
   HANDLE& GetConsoleHandle()
   {
      static HANDLE __handle = GetStdHandle(STD_OUTPUT_HANDLE);
      return __handle;
   }

   HWND& GetConsoleHWND()
   {
      static HWND __hwnd = GetConsoleWindow();
      return __hwnd;
   }
}

ConsoleInterop& ConsoleInterop::Instance()
{
   static ConsoleInterop __instance;
   return __instance;
}

void ConsoleInterop::init()
{
   // the callLock ensures that the init functions only runs once
   static bool callLock = false;
   if (callLock) return;
   callLock = true;

   connect(&m_consoleWatcher, &QTimer::timeout, this, &ConsoleInterop::checkSizes);
   connect(&m_keyWatcher, qOverload<char>(&KeyWatcher::keyPressed), this, qOverload<char>(&ConsoleInterop::keyPressEvent));
   connect(&m_keyWatcher, qOverload<KeyModifier, KeyCode>(&KeyWatcher::keyPressed), this, qOverload<KeyModifier, KeyCode>(&ConsoleInterop::keyPressEvent));

   m_consoleWatcher.setInterval(100);

   m_consoleWatcher.start();
   m_keyWatcher.start();
   setConsoleOutputColor(m_outColor);
}

QSize ConsoleInterop::gridSize()
{
   CONSOLE_SCREEN_BUFFER_INFO csbi;
   GetConsoleScreenBufferInfo(GetConsoleHandle(), &csbi);
   const auto cols = csbi.srWindow.Right - csbi.srWindow.Left + 1;
   const auto rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;

   return QSize(cols, rows);
}

QRect ConsoleInterop::windowRect()
{
   RECT rect;
   GetWindowRect(GetConsoleHWND(), &rect);
   return QRect(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
}

void ConsoleInterop::setCursorPosition(const QPoint& pos)
{
   if (SetConsoleCursorPosition(GetConsoleHandle(), {static_cast<short>(pos.x()), static_cast<short>(pos.y())}))
      m_cursorPos = pos;
}

void ConsoleInterop::startWatcher()
{
   m_consoleWatcher.start();
}

void ConsoleInterop::stopWatcher()
{
   m_consoleWatcher.stop();
}

void ConsoleInterop::checkSizes()
{
	const auto newRect = windowRect();

	const QPoint position(newRect.left(), newRect.top());
	const QPoint lastPosition(m_windowRect.left(), m_windowRect.top());

	const QSize size = newRect.size();
	const QSize lastSize = m_windowRect.size();

   m_windowRect = newRect;
   m_gridSize = gridSize();

   if (position != lastPosition)
   {
      emit consoleWindowPositionChanged(position);
   }

   if (size != lastSize)
   {
      emit consoleWindowSizeChanged(size, m_gridSize);
   }
}

ConsoleInterop::ConsoleInterop()
   : m_consoleWatcher(nullptr)
   , m_keyWatcher(nullptr)
{
   init();
}

const QPoint& ConsoleInterop::getCursorPosition() const
{
   return m_cursorPos;
}

void ConsoleInterop::write(const QString& text)
{
   std::cout << text.toLatin1().constData();
}

QPoint ConsoleInterop::checkCursorPosition(std::size_t count) const
{
   return m_cursorPos + QPoint(count - 1, 0);
}

void ConsoleInterop::setConsoleOutputColor(DisplayColor color)
{
   m_outColor = color;
   SetConsoleTextAttribute(GetConsoleHandle(), static_cast<WORD>(m_outColor));
}

void ConsoleInterop::resetConsoleOutputColor()
{
   setConsoleOutputColor(DisplayColor(Black_Background | White_Foreground));
}

DisplayColor ConsoleInterop::getCurrentForegroundColor() const
{
   return DisplayColor(m_outColor ^ getCurrentBackgroundColor());
}

DisplayColor ConsoleInterop::getCurrentBackgroundColor() const
{
   return DisplayColor(m_outColor >> 4 << 4);
}
