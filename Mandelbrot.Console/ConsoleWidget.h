/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

// Mandelbrot.Console
#include "KeyWatcher.h"

namespace Mandelbrot::Console
{
   /// @brief                    a widget class just like QWidget but for console only
   class ConsoleWidget : public QObject
   {
      Q_OBJECT;

   public: /*typedefs*/

      enum LayoutStyle { Horizontal, Vertical };

   public: /*ctors*/

      ConsoleWidget();

   signals:

      /// @brief                 emitted when the user "clicks" this widget (selected through arrow keys and pressed enter)
      void clicked();

      /// @brief                 emitted when the widget has to update
      /// @note                  use setPosition(const QRect&) to set the cursor position
      /// @note                  use write(const QString&) to write out your widgets as symbols
      /// @note                  use resetCursor() to reset the cursor position afterwards
      void update();

      /// @brief                 emitted when the window has resized
      /// @param size            the new drawable size of this widget
      /// @note                  you can always get the drawable size by calling getDrawableSize()
      void resize(const QSize& size);

      /// @brief                 emitted when the user has pressed a key
      /// @param key             the key that has been pressed as char
      void keyPressed(char key);

      /// @brief                 emitted when the user has pressed a key
      /// @param modifier        the key modifier that has been pressed along with the key
      /// @param key             the key that has been pressed as KeyCode
      void keyPressed(KeyModifier modifier, KeyCode key);

   public: /*methods*/

      /// @brief                 shows the widget
      /// @note                  only use this function on the main widget!
      void show();

      /// @brief                 get all children if this widget
      /// @returns               the list of all children of this widget
      const std::vector<ConsoleWidget*>& getChildren() const;

      /// @brief                 add a widget to the list of children
      /// @param child           the widget to add to the list of children
      void addChild(ConsoleWidget* child);

      /// @brief                 set the layout style this widget should have (Horizontal / Vertical)
      /// @param layoutStyle     the new layout style
      void setLayoutStyle(LayoutStyle layoutStyle);

      /// @brief                 get the current layout style
      /// @returns               the current layout style
      LayoutStyle getLayoutStyle() const;

      /// @brief                 get the total rect of this widget
      /// @note                  to get only the rect that can be used to print something use getDrawableRect()
      /// @returns               the total rect of this widget
      const QRect& getRect() const;

      /// @brief                 get the drawable rect of this widget
      /// @note                  to get the total rect that this widget occupies
      /// @returns               the drawable rect of this widget
      QRect getDrawableRect() const;

   public slots:

      /// @brief                 forces this widget and all children to repaint everything
      void updateChildren();

      /// @brief                 forces this widget and all children to resize correctly
      void resizeChildren(const QSize& size);

   protected: /*methods*/

      /// @brief                 set the cursor position for writing to the console screen
      /// @param pos             the position for the cursor
      static void setPosition(const QPoint& pos);

      /// @brief                 writes the given string to the console screen
      /// @note                  returns false if the string could not be written because of missing space (check getDrawableRect() before)
      /// @param str             the string to write to the console screen
      /// @returns               whether or not the text could be written to the console screen
      bool write(const QString& str) const;

      /// @brief                 writes the given char to the console screen
      /// @note                  returns false if the string could not be written because of missing space (check getDrawableRect() before)
      /// @param ch              the character to write
      /// @returns               whether or not the text could be written to the console screen
      bool write(char ch);

      /// @brief                 sets a border character for this widget or disables the border
      /// @param useBorder       whether or not to use a border
      /// @param borderChar      the character to display as a border
      void setBorder(bool useBorder, char borderChar = ' ');

      /// @brief                 clears the screen this widget occupies
      /// @note                  same as "fill(getRect(), ' ');"
      void clearScreen() const;

      /// @brief                 draws the border
      /// @note                  use setBorder(bool, char) to set the border
      void drawBorder();

      /// @brief                 fills the given rect with the specified char
      /// @param rect            the rect to fill
      /// @param char            the character to use to fill the rect
      void fill(const QRect& rect, char character) const;
      
      /// @brief                 resets the cursor to the position the user has set it to
      void resetCursor() const;

   private: /*methods*/

      /// @brief                 calculates the rect this child may occupy
      /// @param child           the child to take into consideration
      /// @returns               the rect for this child
      QRect calculateChildRect(ConsoleWidget* child) const;

      /// @brief                 calculates the rect for this child if the layout was set to horizontal
      /// @param child           the child to take into consideration
      /// @returns               the rect for this child
      QRect calculateChildRectHorizontal(ConsoleWidget* child) const;

      /// @brief                 calculates the rect for this child if the layout was set to vertical
      /// @param child           the child to take into consideration
      /// @returns               the rect for this child
      QRect calculateChildRectVertical(ConsoleWidget* child) const;

      /// @brief                 intern write method that doesn't check the bounds
      /// @param str             the string to write to the console screen
      static void internWrite(const QString& str);

      /// @brief                 intern write method that doesn't check the bounds
      /// @param str             the character to write to the console screen
      static void internWrite(char ch);

      /// @brief                 sends the click event to the correct widget (either this widget or the child widget at this position)
      /// @param pos             where the click have happened
      void sendClick(const QPoint& pos);

      /// @brief                 sets the rect this widget may occupy
      /// @note                  causes a resize event on this widget and all children
      /// @param rect            the rect this widget may occupy
      void setRect(const QRect& rect);

   private: /*members*/

      std::vector<ConsoleWidget*> m_children = {};
      LayoutStyle m_layoutStyle = { Horizontal };
      QRect m_rect = {0,0,0,0};
      char m_borderChar = ' ';
      bool m_hasBorder = false;
      bool m_isMainWidget = false;
   };
}
