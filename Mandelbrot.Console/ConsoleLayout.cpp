/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ConsoleLayout.h"
using namespace Mandelbrot::Console;

ConsoleLayout::ConsoleLayout(LayoutStyle style)
{
   setLayoutStyle(style);
}
