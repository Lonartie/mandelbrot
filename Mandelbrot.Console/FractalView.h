/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/26                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ConsoleWidget.h"

namespace Mandelbrot::Console
{
   /// @brief           the console view for displaying a fractal (like the mandelbrot or julia set)
   class FractalView: public ConsoleWidget
   {
      Q_OBJECT;

   public: /*ctors*/

      /// @brief        initializes the ui and connecting events
      FractalView();

   private slots:

      /// @brief        draw the title of the displayed view
      /// @note         connected with the update() event to draw every draw-call
      void drawTitle() const;

      /// @brief        test title for click integration
      void drawOtherTitle();

   protected: /*abstract methods*/

      /// @brief        the title of the implementor
      virtual QString getTitle() const = 0;

   private: /*members*/

      /// @brief        the click counter for click tests
      int m_clickedCounter = 0;

   };
}