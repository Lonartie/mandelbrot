/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.Console
#include "KeyWatcher.h"
#include <conio.h>

using namespace Mandelbrot::Console;

KeyWatcher::KeyWatcher(QObject* parent /*= Q_NULLPTR*/)
    : QThread(parent)
{
   qRegisterMetaType<KeyCode>("KeyCode");
   qRegisterMetaType<KeyModifier>("KeyModifier");
}

void KeyWatcher::run()
{
   while (true)
   {
      int k1 = 0;
      int k2 = 0;

      k1 = _getch();
      if (k1 == 224) k2 = _getch();
      
      if (k2 == 0)
      {
         emit keyPressed(k1);
         normalKeyInput(k1);
      }
      else
      {
         emit keyPressed(k2);
         specialKeyInput(k2);
      }
   }
}

void KeyWatcher::normalKeyInput(int input)
{
   switch (static_cast<char>(input))
   {
      // numbers
   case '1': return emit keyPressed(KeyModifier::_none, KeyCode::_1);
   case '2': return emit keyPressed(KeyModifier::_none, KeyCode::_2);
   case '3': return emit keyPressed(KeyModifier::_none, KeyCode::_3);
   case '4': return emit keyPressed(KeyModifier::_none, KeyCode::_4);
   case '5': return emit keyPressed(KeyModifier::_none, KeyCode::_5);
   case '6': return emit keyPressed(KeyModifier::_none, KeyCode::_6);
   case '7': return emit keyPressed(KeyModifier::_none, KeyCode::_7);
   case '8': return emit keyPressed(KeyModifier::_none, KeyCode::_8);
   case '9': return emit keyPressed(KeyModifier::_none, KeyCode::_9);
      // characters
   case 'a': return emit keyPressed(KeyModifier::_none, KeyCode::_a);
   case 'b': return emit keyPressed(KeyModifier::_none, KeyCode::_b);
   case 'c': return emit keyPressed(KeyModifier::_none, KeyCode::_c);
   case 'd': return emit keyPressed(KeyModifier::_none, KeyCode::_d);
   case 'e': return emit keyPressed(KeyModifier::_none, KeyCode::_e);
   case 'f': return emit keyPressed(KeyModifier::_none, KeyCode::_f);
   case 'g': return emit keyPressed(KeyModifier::_none, KeyCode::_g);
   case 'h': return emit keyPressed(KeyModifier::_none, KeyCode::_h);
   case 'i': return emit keyPressed(KeyModifier::_none, KeyCode::_i);
   case 'j': return emit keyPressed(KeyModifier::_none, KeyCode::_j);
   case 'k': return emit keyPressed(KeyModifier::_none, KeyCode::_k);
   case 'l': return emit keyPressed(KeyModifier::_none, KeyCode::_l);
   case 'm': return emit keyPressed(KeyModifier::_none, KeyCode::_m);
   case 'n': return emit keyPressed(KeyModifier::_none, KeyCode::_n);
   case 'o': return emit keyPressed(KeyModifier::_none, KeyCode::_o);
   case 'p': return emit keyPressed(KeyModifier::_none, KeyCode::_p);
   case 'q': return emit keyPressed(KeyModifier::_none, KeyCode::_q);
   case 'r': return emit keyPressed(KeyModifier::_none, KeyCode::_r);
   case 's': return emit keyPressed(KeyModifier::_none, KeyCode::_s);
   case 't': return emit keyPressed(KeyModifier::_none, KeyCode::_t);
   case 'u': return emit keyPressed(KeyModifier::_none, KeyCode::_u);
   case 'v': return emit keyPressed(KeyModifier::_none, KeyCode::_v);
   case 'w': return emit keyPressed(KeyModifier::_none, KeyCode::_w);
   case 'x': return emit keyPressed(KeyModifier::_none, KeyCode::_x);
   case 'y': return emit keyPressed(KeyModifier::_none, KeyCode::_y);
   case 'z': return emit keyPressed(KeyModifier::_none, KeyCode::_z);
   case '�': return emit keyPressed(KeyModifier::_none, KeyCode::_ae);
   case '�': return emit keyPressed(KeyModifier::_none, KeyCode::_oe);
   case '�': return emit keyPressed(KeyModifier::_none, KeyCode::_ue);
      // upper case characters
   case 'A': return emit keyPressed(KeyModifier::_shift, KeyCode::_a);
   case 'B': return emit keyPressed(KeyModifier::_shift, KeyCode::_b);
   case 'C': return emit keyPressed(KeyModifier::_shift, KeyCode::_c);
   case 'D': return emit keyPressed(KeyModifier::_shift, KeyCode::_d);
   case 'E': return emit keyPressed(KeyModifier::_shift, KeyCode::_e);
   case 'F': return emit keyPressed(KeyModifier::_shift, KeyCode::_f);
   case 'G': return emit keyPressed(KeyModifier::_shift, KeyCode::_g);
   case 'H': return emit keyPressed(KeyModifier::_shift, KeyCode::_h);
   case 'I': return emit keyPressed(KeyModifier::_shift, KeyCode::_i);
   case 'J': return emit keyPressed(KeyModifier::_shift, KeyCode::_j);
   case 'K': return emit keyPressed(KeyModifier::_shift, KeyCode::_k);
   case 'L': return emit keyPressed(KeyModifier::_shift, KeyCode::_l);
   case 'M': return emit keyPressed(KeyModifier::_shift, KeyCode::_m);
   case 'N': return emit keyPressed(KeyModifier::_shift, KeyCode::_n);
   case 'O': return emit keyPressed(KeyModifier::_shift, KeyCode::_o);
   case 'P': return emit keyPressed(KeyModifier::_shift, KeyCode::_p);
   case 'Q': return emit keyPressed(KeyModifier::_shift, KeyCode::_q);
   case 'R': return emit keyPressed(KeyModifier::_shift, KeyCode::_r);
   case 'S': return emit keyPressed(KeyModifier::_shift, KeyCode::_s);
   case 'T': return emit keyPressed(KeyModifier::_shift, KeyCode::_t);
   case 'U': return emit keyPressed(KeyModifier::_shift, KeyCode::_u);
   case 'V': return emit keyPressed(KeyModifier::_shift, KeyCode::_v);
   case 'W': return emit keyPressed(KeyModifier::_shift, KeyCode::_w);
   case 'X': return emit keyPressed(KeyModifier::_shift, KeyCode::_x);
   case 'Y': return emit keyPressed(KeyModifier::_shift, KeyCode::_y);
   case 'Z': return emit keyPressed(KeyModifier::_shift, KeyCode::_z);
   case '�': return emit keyPressed(KeyModifier::_shift, KeyCode::_ae);
   case '�': return emit keyPressed(KeyModifier::_shift, KeyCode::_oe);
   case '�': return emit keyPressed(KeyModifier::_shift, KeyCode::_ue);
      // special characters
   case 13: return emit keyPressed(KeyModifier::_none, KeyCode::_return);
   case ' ': return emit keyPressed(KeyModifier::_none, KeyCode::_space);
   case '.': return emit keyPressed(KeyModifier::_none, KeyCode::_dot);
   case ',': return emit keyPressed(KeyModifier::_none, KeyCode::_comma);
   case '+': return emit keyPressed(KeyModifier::_none, KeyCode::_plus);
   case '-': return emit keyPressed(KeyModifier::_none, KeyCode::_minus);
   }                                
}

void KeyWatcher::specialKeyInput(int input)
{
   switch(input)
   {
      // arrow keys
   case 72: return emit keyPressed(KeyModifier::_none, KeyCode::_arrow_up);
   case 141: return emit keyPressed(KeyModifier::_ctrl, KeyCode::_arrow_up);
   case 80: return emit keyPressed(KeyModifier::_none, KeyCode::_arrow_down);
   case 145: return emit keyPressed(KeyModifier::_ctrl, KeyCode::_arrow_down);
   case 75: return emit keyPressed(KeyModifier::_none, KeyCode::_arrow_left);
   case 115: return emit keyPressed(KeyModifier::_ctrl, KeyCode::_arrow_left);
   case 77: return emit keyPressed(KeyModifier::_none, KeyCode::_arrow_right);
   case 116: return emit keyPressed(KeyModifier::_ctrl, KeyCode::_arrow_right);
   }
}
