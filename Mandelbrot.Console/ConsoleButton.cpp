/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "ConsoleButton.h"
using namespace Mandelbrot::Console;

ConsoleButton::ConsoleButton(const QString& text)
   : m_text(text)
{
   setBorder(true, '*');
   connect(this, &ConsoleWidget::update, this, &ConsoleButton::drawButton);
}

void ConsoleButton::setText(const QString& text)
{
   m_text = text;
}

const QString& ConsoleButton::getText() const
{
   return m_text;
}

void ConsoleButton::drawButton()
{
   // center line of drawable rect
   const auto rect = getDrawableRect();
   const QRect nrect(rect.left(), rect.top() + rect.height() / 2, rect.width(), 1);

   // center text
   const auto left = (nrect.width() - m_text.count()) / 2;

   setPosition(nrect.topLeft() + QPoint(left, 0));
   write(m_text);
}
