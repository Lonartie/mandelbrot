/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

// Mandelbrot.Console
#include "KeyWatcher.h"

namespace Mandelbrot::Console
{
   enum DisplayColor : unsigned int
   {
      Black_Foreground        =  0,             Black_Background        = Black_Foreground         << 4,
      DarkBlue_Foreground     =  1,             DarkBlue_Background     = DarkBlue_Foreground      << 4,
      DarkGreen_Foreground    =  2,             DarkGreen_Background    = DarkGreen_Foreground     << 4,
      DarkCyan_Foreground     =  3,             DarkCyan_Background     = DarkCyan_Foreground      << 4,
      DarkRed_Foreground      =  4,             DarkRed_Background      = DarkRed_Foreground       << 4,
      DarkMagenta_Foreground  =  5,             DarkMagenta_Background  = DarkMagenta_Foreground   << 4,
      DarkYellow_Foreground   =  6,             DarkYellow_Background   = DarkYellow_Foreground    << 4,
      Gray_Foreground         =  7,             Gray_Background         = Gray_Foreground          << 4,
      DarkGray_Foreground     =  8,             DarkGray_Background     = DarkGray_Foreground      << 4,
      Blue_Foreground         =  9,             Blue_Background         = Blue_Foreground          << 4,
      Green_Foreground        = 10,             Green_Background        = Green_Foreground         << 4,
      Cyan_Foreground         = 11,             Cyan_Background         = Cyan_Foreground          << 4,
      Red_Foreground          = 12,             Red_Background          = Red_Foreground           << 4,
      Magenta_Foreground      = 13,             Magenta_Background      = Magenta_Foreground       << 4,
      Yellow_Foreground       = 14,             Yellow_Background       = Yellow_Foreground        << 4,
      White_Foreground        = 15,             White_Background        = White_Foreground         << 4,
   };

   /// @brief              provides interoperability between the arbitrary C++ code and the windows console
   class ConsoleInterop : public QObject
   {
      Q_OBJECT;

   public: /*static methods*/

      /// @brief           provides access to the static singleton
      /// @note            this is and should be the only instance in the application!
      static ConsoleInterop& Instance();

   signals:

      /// @brief           is emitted when the console window has moved (-> the position has changed)
      /// @param position  the new console window position
      void consoleWindowPositionChanged(const QPoint& position);

      /// @brief           is emitted when the console window has been resized (-> size has changed)
      /// @param pixelSize the new window size in pixel
      /// @param gridSize  the new window size in grid (columns and rows)
      void consoleWindowSizeChanged(const QSize& pixelSize, const QSize& gridSize);

      /// @brief           is emitted when a key has been pressed
      /// @param modifier  the key modifier (ctrl, alt, shift, etc...)
      /// @param key       the actual key (a, left arrow, space, return)
      void keyPressEvent(KeyModifier modifier, KeyCode key);

      /// @brief           is emitted when a key has been pressed
      /// @param key       the actual key as char
      void keyPressEvent(char key);

   public slots:

      /// @brief           starts the console watcher (needed for events to be detected)
      /// @note            the console watcher starts automatically
      void startWatcher();

      /// @brief           stops the console watcher
      void stopWatcher();

      /// @brief           sets the cursor position in the console window
      /// @param pos       the position in grid space
      void setCursorPosition(const QPoint& pos);

      /// @brief           set the console output color
      /// @note            default color is always black!
      /// @param col       the color (may contain foreground and background color [foreground | background])
      void setConsoleOutputColor(DisplayColor color);

      /// @brief           resets the console output color to black background white foreground (default)
      void resetConsoleOutputColor();

      /// @brief           get the current foreground color
      /// @returns         the current foreground color
      DisplayColor getCurrentForegroundColor() const;

      /// @brief           get the current background color
      /// @returns         the current background color
      DisplayColor getCurrentBackgroundColor() const;

   public: /*methods*/

      /// @brief           initializes the ConsoleWatcher so that signals can be emitted
      void init();

      /// @brief           get the console grid size in rows and columns
      static QSize gridSize();

      /// @brief           get the actual console window rect in pixel
      static QRect windowRect();

      /// @brief           get the current cursor position in grid space
      const QPoint& getCursorPosition() const;

      /// @brief           writes the given \a text to the console
      static void write(const QString& text);

      /// @brief           get the point of the cursor if you would write this many characters to the screen
      /// @note            this function does NOT take the next line mechanism into account
      /// @param count     the number of characters you plan to write to the console
      /// @returns         the point where the cursor would be after writing to the console
      QPoint checkCursorPosition(std::size_t count) const;

   private slots:

      /// @brief           checks whether or not the console has changed and emits the appropriate signals
      void checkSizes();

   private: /*ctors*/

      ConsoleInterop();
      ~ConsoleInterop() = default;

      ConsoleInterop(const ConsoleInterop&) = delete;
      ConsoleInterop(ConsoleInterop&&) = delete;

   private: /*members*/

      QTimer m_consoleWatcher;
      KeyWatcher m_keyWatcher;
      QRect m_windowRect = {0,0,0,0};
      QSize m_gridSize = {0,0};
      QPoint m_cursorPos = {0,0};
      DisplayColor m_outColor = DisplayColor(Black_Background | White_Foreground);

   };
}