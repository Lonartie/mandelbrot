/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ConsoleWidget.h"

namespace Mandelbrot::Console
{
   /// @brief           a layout class to tile the current layout
   class ConsoleLayout: public ConsoleWidget
   {
      Q_OBJECT;

   public: /*ctors*/
      
      /// @brief        initializes the layout with the specified layout style
      ConsoleLayout(LayoutStyle style);

   };
}