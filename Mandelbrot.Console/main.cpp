/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/22                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.Console
#include "Dashboard.h"
#include "ConsoleInterop.h"
#include "CursorEmulator.h"
using namespace Mandelbrot::Console;

int main(int argc, char *argv[])
{
   QCoreApplication app(argc, argv);
   ConsoleInterop::Instance().init();
   CursorEmulator::Instance().init();

   Dashboard dashboard;
   dashboard.show();

   return app.exec();
}
