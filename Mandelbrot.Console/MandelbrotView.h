/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "FractalView.h"

namespace Mandelbrot::Console
{
   /// @brief        the console view for the mandelbrot fractal
   class MandelbrotView: public FractalView
   {
      Q_OBJECT;

   protected: /*implemented methods from FractalView*/

      /// @copydoc   Mandelbrot::Console::FractalView::getTitle()
      QString getTitle() const override;

   };
}