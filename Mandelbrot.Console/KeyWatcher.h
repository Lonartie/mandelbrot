/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"

namespace Mandelbrot::Console
{
   /// @brief              an enum containing the mostly used keys 
   enum class KeyCode
   {
      // normal keys
      _a, _b, _c, _d, _e, _f, _g, _h, _i, _j, _k, _l, _m, _n, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _ae, _oe, _ue,
      // numbers
      _0, _1, _2, _3, _4, _5, _6, _7, _8, _9,
      // special
      _return, _space, _dot, _comma, _minus, _plus,
      // arrows
      _arrow_up, _arrow_down, _arrow_left, _arrow_right,
   };

   /// @brief              an enum containing every possible key modifier like shift, ctrl, etc...
   enum class KeyModifier
   {
      _none, _ctrl, _shift, _alt, _altr_gr
   };

   /// @brief              watching the console for any pressed keys
   class KeyWatcher: public QThread
   {
      Q_OBJECT;

   public: /*ctors*/

      /// @brief           initializes the object by registering the meta types for Qt connect mechanism
      KeyWatcher(QObject* parent = Q_NULLPTR);

      /// @brief           defaulted destructor
      ~KeyWatcher() = default;

   signals:

      /// @brief           the given key was pressed along with the specified modifier
      /// @param modifier  the key modifier that has been pressed along with the key
      /// @param key       the key that has been pressed
      void keyPressed(KeyModifier modifier, KeyCode key);

      /// @brief           the given key was pressed
      /// @param key       the key as char
      void keyPressed(char key);

   protected: /*methods*/

      /// @brief           executed when the QThread has been started
      /// @note            this starts the mechanism which is waiting for a key in an endless loop (until killed)
      void run() override;

   private: /*methods*/

      /// @brief           processes the input for normal keys
      /// @param input     the key input
      void normalKeyInput(int input);

      /// @brief           processes the input for special keys
      /// @param input     the special key input
      void specialKeyInput(int input);
   };
}

Q_DECLARE_METATYPE(Mandelbrot::Console::KeyCode*);
Q_DECLARE_METATYPE(Mandelbrot::Console::KeyModifier*);
Q_DECLARE_METATYPE(Mandelbrot::Console::KeyCode);
Q_DECLARE_METATYPE(Mandelbrot::Console::KeyModifier);