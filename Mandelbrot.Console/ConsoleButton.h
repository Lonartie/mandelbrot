/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "ConsoleWidget.h"

namespace Mandelbrot::Console
{
   /// @brief              a console button class to display a button as a console widget
   class ConsoleButton: public ConsoleWidget
   {
      Q_OBJECT;

   public: /*ctors*/

      /// @brief           initializes the button with the given display text
      /// @note            the text can be changed afterwards with setText(const QString&)
      /// @param text      the text that should be displayed on the button
      ConsoleButton(const QString& text);

   public: /*methods*/

      /// @brief           set the text that should be displayed
      /// @param text      the text that should be displayed
      void setText(const QString& text);

      /// @brief           get the displayed text
      /// @returns         the displayed text
      const QString& getText() const;

   private slots:

      /// @brief           draws the button
      /// @note            the constructor connects it with the update() signal
      void drawButton();

   private: /*members*/

      /// @brief           the member that stores the text that will be displayed
      QString m_text;
   };
}