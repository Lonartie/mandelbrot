/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/26                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "stdafx.h"
#include "KeyWatcher.h"

namespace Mandelbrot::Console
{
   /// @brief              emulates the cursor by binding the arrow keys to move the position dot in the console
   class CursorEmulator : public QObject
   {
      Q_OBJECT;

   public: /*static methods*/

      /// @brief           access to the singleton object
      /// @returns         the one and only instance of this class
      static CursorEmulator& Instance();

   public: /*methods*/

      /// @brief           initializes the cursor emulator which can be moved by the arrow keys
      void init();

      /// @brief           resets the cursor position to the last known position
      void reset() const;

      /// @brief           resets the cursor position to the last known position
      /// @note            if the position is outsize the bounds reset to bounds top left
      /// @param bounds    the bounds the cursor should be in
      void reset(const QRect& bounds);

   signals:

      /// @brief           emitted when the user clicks a position (presses enter at the cursor position)
      void clicked(const QPoint& pos);

   private slots:

      /// @brief           checks whether the key was an arrow key or not (to move the cursor)
      /// @note            to access an instance use the static singleton by calling Instance()
      void checkKeyInput(KeyModifier modifier, KeyCode key);

   private: /*ctors*/

      /// @brief           initializes this instance by connecting all events (private only)
      CursorEmulator();

      /// @brief           deleted copy constructor
      CursorEmulator(const CursorEmulator&) = delete;

      /// @brief           deleted move constructor
      CursorEmulator(CursorEmulator&&) = delete;

      /// @brief           defaulted destructor (private only)
      ~CursorEmulator() = default;

   private: /*methods*/

      /// @brief           moves the cursor by \a delta
      /// @param delta     the columns / rows to move
      void move(const QPoint& delta);

   private: /*members*/

      /// @brief           the last known cursor position
      /// @note            the cursor position changes whenever something was drawn to the console screen
      QPoint m_lastPos = {0,0};

   };
}