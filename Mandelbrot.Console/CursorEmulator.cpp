/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/26                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "CursorEmulator.h"
#include "ConsoleInterop.h"
using namespace Mandelbrot::Console;

CursorEmulator& CursorEmulator::Instance()
{
   static CursorEmulator __instance;
   return __instance;
}

CursorEmulator::CursorEmulator()
{
   init();
}

void CursorEmulator::init()
{
   static bool inited = false;
   if (inited) return;
   inited = true;

   connect(&ConsoleInterop::Instance(), qOverload<KeyModifier, KeyCode>(&ConsoleInterop::keyPressEvent), this, &CursorEmulator::checkKeyInput);
}

void CursorEmulator::checkKeyInput(KeyModifier modifier, KeyCode key)
{
   int speed = 1;

   if (modifier == KeyModifier::_ctrl) speed = 10;

   if (key == KeyCode::_arrow_up) return move({0 * speed, -1 * speed});
   if (key == KeyCode::_arrow_down) return move({0 * speed, 1 * speed});
   if (key == KeyCode::_arrow_left) return move({-1 * speed, 0 * speed});
   if (key == KeyCode::_arrow_right) return move({1 * speed, 0 * speed});

   if (key == KeyCode::_return) 
      return emit clicked(ConsoleInterop::Instance().getCursorPosition());
}

void CursorEmulator::move(const QPoint& delta)
{
	const auto pos = ConsoleInterop::Instance().getCursorPosition();
	const auto newPos = pos + delta;
   ConsoleInterop::Instance().setCursorPosition(newPos);
   m_lastPos = ConsoleInterop::Instance().getCursorPosition();
}

void CursorEmulator::reset() const
{
   ConsoleInterop::Instance().setCursorPosition(m_lastPos);
}

void CursorEmulator::reset(const QRect& bounds)
{
   if (!bounds.contains(m_lastPos))
      m_lastPos = bounds.topLeft();

   reset();
}
