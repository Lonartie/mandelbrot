/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Dashboard.h"
using namespace Mandelbrot::Console;

Dashboard::Dashboard()
   : m_fractals(Horizontal)
   , m_buttons(Horizontal)
   , m_button("CLICK HERE...")
{
   setLayoutStyle(Vertical);
   
   m_fractals.addChild(&m_mandelbrot);
   m_fractals.addChild(&m_julia);
   m_buttons.addChild(&m_button);

   addChild(&m_fractals);
   addChild(&m_buttons);

   connect(&m_button, &ConsoleButton::clicked, &m_mandelbrot, &FractalView::clicked);
   connect(&m_button, &ConsoleButton::clicked, &m_julia, &FractalView::clicked);
}

