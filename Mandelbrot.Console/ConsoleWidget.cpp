/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/25                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

// Mandelbrot.Console
#include "ConsoleWidget.h"
#include "ConsoleInterop.h"
#include "CursorEmulator.h"

// STD
#include <iostream>
#include <string>
using namespace Mandelbrot::Console;

void ConsoleWidget::show()
{
   static bool __shown = false;
   if (__shown) return;
   __shown = true;

   connect(&ConsoleInterop::Instance(), &ConsoleInterop::consoleWindowSizeChanged, std::bind(&ConsoleWidget::resizeChildren, this, std::placeholders::_2));
   connect(&CursorEmulator::Instance(), &CursorEmulator::clicked, this, &ConsoleWidget::sendClick);

   m_isMainWidget = true;
   updateChildren();
}

const std::vector<ConsoleWidget*>& ConsoleWidget::getChildren() const
{
   return m_children;
}

void ConsoleWidget::addChild(ConsoleWidget* child)
{
   m_children.push_back(child);
}

void ConsoleWidget::updateChildren()
{
   if (m_isMainWidget)
      clearScreen();

   if (m_hasBorder)
      drawBorder();

   emit update();
   for (auto& child : m_children)
      child->updateChildren();

   if (m_isMainWidget)
      resetCursor();
}

void ConsoleWidget::resizeChildren(const QSize& size)
{
   m_rect.setSize(size);

   emit resize(size);
   for (auto& child : m_children)
      child->setRect(calculateChildRect(child));

   updateChildren();
}

const QRect& ConsoleWidget::getRect() const
{
   return m_rect;
}

void ConsoleWidget::setLayoutStyle(LayoutStyle layoutStyle)
{
   m_layoutStyle = layoutStyle;
}

ConsoleWidget::LayoutStyle ConsoleWidget::getLayoutStyle() const
{
   return m_layoutStyle;
}

void ConsoleWidget::setPosition(const QPoint& pos)
{
   ConsoleInterop::Instance().setCursorPosition(pos);
}

bool ConsoleWidget::write(const QString& str) const
{
	const auto pos = ConsoleInterop::Instance().getCursorPosition();
	const auto future_pos = ConsoleInterop::Instance().checkCursorPosition(str.count());

   if (!getDrawableRect().contains(pos))
      return false;

   if (!getDrawableRect().contains(future_pos))
      return false;

   internWrite(str);
   return true;
}

bool ConsoleWidget::write(char ch)
{
	const auto pos = ConsoleInterop::Instance().getCursorPosition();

   if (!getDrawableRect().contains(pos))
      return false;

   internWrite(ch);
   return true;
}

void ConsoleWidget::clearScreen() const
{
   fill(m_rect, ' ');
}

void ConsoleWidget::fill(const QRect& rect, char character) const
{
   QString line;
   for (auto x = 0; x < rect.width(); x++)
      line += character;

   for (auto y = 0; y < rect.height(); y++)
   {
      setPosition({rect.left(), rect.top() + y});
      internWrite(line);
   }
}

QRect ConsoleWidget::calculateChildRect(ConsoleWidget* child) const
{
   switch (m_layoutStyle)
   {
   case Horizontal: return calculateChildRectHorizontal(child);
   case Vertical: return calculateChildRectVertical(child);
   }

   return {0,0,0,0};
}

QRect ConsoleWidget::calculateChildRectHorizontal(ConsoleWidget* child) const
{
	const auto iter = std::find(m_children.begin(), m_children.end(), child);
   std::size_t index = 0;

   if (iter == m_children.end())
      return {0,0,0,0};

   index = std::distance(m_children.begin(), iter);

	const auto width = m_rect.width() / m_children.size();
	const auto height = m_rect.height();
	const auto y = m_rect.top();
	const auto x = m_rect.left() + width * index;

   return QRect(x, y, width, height);
}

QRect ConsoleWidget::calculateChildRectVertical(ConsoleWidget* child) const
{
	const auto iter = std::find(m_children.begin(), m_children.end(), child);
   std::size_t index = 0;

   if (iter == m_children.end())
      return {0,0,0,0};

   index = std::distance(m_children.begin(), iter);

	const auto width = m_rect.width();
	const auto height = m_rect.height() / m_children.size();
	const auto y = m_rect.top() + height * index;
	const auto x = m_rect.left();

   return QRect(x, y, width, height);
}

void ConsoleWidget::internWrite(const QString& str)
{
   ConsoleInterop::Instance().write(str);
}

void ConsoleWidget::internWrite(char ch)
{
   ConsoleInterop::Instance().write(QString(ch));
}

void ConsoleWidget::sendClick(const QPoint& pos)
{
   if (!m_rect.contains(pos))
      return;

   if (!m_children.size())
      return emit clicked();

   bool found = false;
   for (auto& child : m_children)
      if (child->m_rect.contains(pos))
      {
         found = true;
         emit child->sendClick(pos);
      }

   if (!found)
      return emit clicked();
}

ConsoleWidget::ConsoleWidget()
{
   connect(&ConsoleInterop::Instance(), qOverload<char>(&ConsoleInterop::keyPressEvent), this, qOverload<char>(&ConsoleWidget::keyPressed));
   connect(&ConsoleInterop::Instance(), qOverload<KeyModifier, KeyCode>(&ConsoleInterop::keyPressEvent), this, qOverload<KeyModifier, KeyCode>(&ConsoleWidget::keyPressed));
}

void ConsoleWidget::setBorder(bool useBorder, char borderChar /*= ' '*/)
{
   m_hasBorder = useBorder;
   m_borderChar = borderChar;
}

void ConsoleWidget::drawBorder()
{
	const auto rect = getRect();

   for (int y = 0; y < rect.height(); y++)
   {
      setPosition({rect.left(), rect.top() + y});
      internWrite(m_borderChar);
      setPosition({rect.left() + rect.width() - 1, rect.top() + y});
      internWrite(m_borderChar);
   }

   QString line;
   for (int x = 0; x < rect.width(); x++)
      line += m_borderChar;

   setPosition(rect.topLeft());
   internWrite(line);
   setPosition(rect.bottomLeft());
   internWrite(line);

   std::cout.flush();
}

void ConsoleWidget::resetCursor() const
{
   if (m_isMainWidget)
      CursorEmulator::Instance().reset(m_rect);
   else
      CursorEmulator::Instance().reset();
}

QRect ConsoleWidget::getDrawableRect() const
{
   if (!m_hasBorder)
      return m_rect;
   return QRect
   {
	   m_rect.left() + 1,
	   m_rect.top() + 1,
	   m_rect.width() - 2,
	   m_rect.height() - 2
   };
}

void ConsoleWidget::setRect(const QRect& rect)
{
   m_rect.setTopLeft(rect.topLeft());
   resizeChildren(rect.size());
}