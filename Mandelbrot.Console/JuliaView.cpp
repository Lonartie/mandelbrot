/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "JuliaView.h"
using namespace Mandelbrot::Console;

QString JuliaView::getTitle() const
{
   return "Julia-View";
}
