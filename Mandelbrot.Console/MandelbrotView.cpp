/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/27                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "MandelbrotView.h"
using namespace Mandelbrot::Console;

QString MandelbrotView::getTitle() const
{
   return "Mandelbrot-View";
}
