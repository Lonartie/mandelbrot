# Mandelbrot README #

### What is this repository for? ###

* Creating a fast Mandelbrot-set (and Julia-set) viewer

### Legend: ###

* (*~) --> not planned yet (I will inform you when you need that)

### How do I get set up? ###

* install git and clone this repository from here (https://bitbucket.org/Lonartie/Mandelbrot.git)
* install visual studio 2017 (or newer)
* install Qt 5.9.0 (or newer)
* (*install OpenCL SDK from here ())

### How is this project organized? ###

``` 
Mandelbrot (Solution)
|
|--> Mandelbrot.Core (Core project)
|       |
|       |--> Mandelbrot.Console         (Console viewer)
|       |--> Mandelbrot.GUI             (GUI viewer)
|       |--> (*Mandelbrot.Core.Tests)   (Tests application)
```

### Things you should need to know: ###

* All projects compile with at least QtCore
* Only the GUI project has QtGUI and QtWidgets
* All projects compile with C++17
* All ~.cpp files need to include the precompiled header (stdafx.h)

### Rules: ###

* Before you upload your changes, pull the branch and check if everything works!

### CI integration (Jenkins): ###

* After each commit jenkins builds the pushed branch
* You can see the result here (http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/) 

### CI status: ###

* [![Build Status](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/badge/icon)](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/)
* [![CppCheck Status](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/cppcheckResult/graph)](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/lastBuild/cppcheckResult/)
* [![Build Status](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/test/trend)](http://lonartie.ddns.net:55002/job/Mandelbrot.Windows.x64/test_results_analyzer/) 
* Leon Gierschner: [![time tracker](https://wakatime.com/badge/bitbucket/Lonartie/mandelbrot.svg)](https://wakatime.com/badge/bitbucket/Lonartie/mandelbrot)


### Who do I talk to? ###

* If you need any help with the project itself or connection or anything like that, please refer to Leon Gierschner.
 