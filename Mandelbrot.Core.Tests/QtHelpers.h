/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include <QSize>

#define BOOST_CHECK_EQUAL_QSIZE(a, b) BOOST_TEST( a.width(), b.width() ); BOOST_TEST( a.height(), b.height() )

