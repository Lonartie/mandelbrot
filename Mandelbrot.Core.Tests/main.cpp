/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/23                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"

namespace Mandelbrot::Core::Dll
{
   bool MANDELBROTCORE_EXPORT DLL_LOAD;
}

static bool DEPS_LOADED = Mandelbrot::Core::Dll::DLL_LOAD;