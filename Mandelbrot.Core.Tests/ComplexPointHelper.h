/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#include "Mandelbrot.Core/ComplexPoint.h"

#define BOOST_TEST_COMPLEXPOINT(expected, result) BOOST_TEST(expected.real() == result.real()); BOOST_TEST(expected.imag() == result.imag())
#define BOOST_TEST_COMPLEXRECT(expected, result) BOOST_TEST_COMPLEXPOINT(expected.getUpperLeft(), result.getUpperLeft()); BOOST_TEST_COMPLEXPOINT(expected.getLowerRight(), result.getLowerRight())
#define BOOST_TEST_VIEWPOINT(expected, result) BOOST_TEST(expected.x() == result.x()); BOOST_TEST(expected.y() == result.y())
