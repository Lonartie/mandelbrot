/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Mandelbrot.Core/ComplexPoint.h"
using namespace Mandelbrot::Core;
#pragma warning(push)
#pragma warning(disable:4305)

BOOST_AUTO_TEST_SUITE(TS_ComplexPointTests);

BOOST_AUTO_TEST_CASE(ComplexPointPrecisionIsCorrect)
{
   using type_to_check = float;
   constexpr bool result = std::is_same<type_to_check, ComplexPoint::value_type>::value;
   BOOST_CHECK(result);
}

BOOST_AUTO_TEST_CASE(ComplexPointAddition, *boost::unit_test::tolerance<precision_type>(1e-5))
{
	const ComplexPoint a(1.5293, 2.9203);
	const ComplexPoint b(3.1478, 4.7182);
	const ComplexPoint result = a + b;
	const ComplexPoint expected(4.6771, 7.6385);

   BOOST_TEST_COMPLEXPOINT(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointSubtraction, *boost::unit_test::tolerance<precision_type>(1e-5))
{
	const ComplexPoint a(1.5293, 2.9203);
	const ComplexPoint b(3.1478, 4.7182);
	const ComplexPoint result = a - b;
	const ComplexPoint expected(-1.6185, -1.7979);

   BOOST_TEST_COMPLEXPOINT(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointMultiplication, *boost::unit_test::tolerance<precision_type>(1e-5))
{
	const ComplexPoint a(1.5293, 2.9203);
	const ComplexPoint b(3.1478, 4.7182);
	const ComplexPoint result = a * b;
	const ComplexPoint expected(-8.96463, 16.4081);

   BOOST_TEST_COMPLEXPOINT(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointDivision, *boost::unit_test::tolerance<precision_type>(1e-5))
{
	const ComplexPoint a(1.5293, 2.9203);
	const ComplexPoint b(3.1478, 4.7182);
	const ComplexPoint result = a / b;
	const ComplexPoint expected(0.577944, 0.0614539);

   BOOST_TEST_COMPLEXPOINT(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointMagnitude, *boost::unit_test::tolerance<precision_type>(1e-5))
{
	const ComplexPoint a(1.5293, 2.9203);
	const precision_type result = std::abs(a);
   precision_type expected = 3.29649973;

   BOOST_TEST(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointSetMagnitude, *boost::unit_test::tolerance<precision_type>(1e-5))
{
   ComplexPoint a(1.5293, 2.9203);
   a = a / std::abs(a);
   a.real(a.real() * 8);
   a.imag(a.imag() * 8);
   const precision_type result = std::abs(a);
   precision_type expected = 8;

   BOOST_TEST(expected, result);
}

BOOST_AUTO_TEST_CASE(ComplexPointEquality)
{
   ComplexPoint a(1.2345, 6.8273);
   ComplexPoint b(9.2374, 2.2492);
   ComplexPoint c(0.2384, 6.1238);
   ComplexPoint d(4.2034, 8.4549);
   ComplexPoint e(4.2034, 8.4549);

   BOOST_TEST(std::abs(a) < std::abs(b));
   BOOST_TEST(std::abs(b) > std::abs(a));
   BOOST_TEST(a != b);
   BOOST_TEST(std::abs(c) < std::abs(d));
   BOOST_TEST(std::abs(d) > std::abs(c));
   BOOST_TEST(c != d);

   BOOST_TEST(d == e);
   BOOST_TEST(e == d);
   BOOST_TEST(std::abs(d) >= std::abs(e));
   BOOST_TEST(std::abs(e) >= std::abs(d));
   BOOST_TEST(std::abs(d) <= std::abs(e));
   BOOST_TEST(std::abs(e) <= std::abs(d));
}


BOOST_AUTO_TEST_SUITE_END();
#pragma warning(pop)