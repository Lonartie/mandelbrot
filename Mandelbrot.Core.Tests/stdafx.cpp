/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/23                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#define BOOST_TEST_MODULE MandelbrotCoreTests
#include "stdafx.h"