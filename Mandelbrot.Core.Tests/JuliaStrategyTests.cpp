/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Lisa-Marie Hallemann                    */
/*  Date    :   2020/04/14                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Mandelbrot.Core/JuliaCPUStrategy.h"
#include "Mandelbrot.Core/SmoothingStrategy.h"
#include "Mandelbrot.Core/JuliaCPUMultiStrategy.h"
#include "Mandelbrot.Core/JuliaOCLStrategy.h"

using namespace Mandelbrot::Core;
#pragma warning(push)
#pragma warning(disable:4305)

struct JuliaFixture
{
    JuliaFixture()
        : smooth(new SmoothingStrategy)
        , settings(128, *smooth)
    {}

    ~JuliaFixture()
    {
        delete smooth;
    }

    SmoothingStrategy* smooth;
    CalculationSettings settings;
};

void juliaTest(CalculationStrategy* calc)
{
    {
        auto dataset = calc->calculate({ {0, 0}, {0, 0} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(128, dataset.getDataPoint({ 0, 0 }));
    }
    {
        auto dataset = calc->calculate({ {-1, 0.03}, {-1, 0.03} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(10, dataset.getDataPoint({ -1, 0.03 }));
    }
    {
        auto dataset = calc->calculate({ {1, -0.23}, {1, -0.23} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(4, dataset.getDataPoint({ 1, -0.23 }));
    }
    {
        auto dataset = calc->calculate({ {0.003, -1.0073}, {0.003, -1.0073} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(6, dataset.getDataPoint({ 0.003, -1.0073 }));
    }
    {
        auto dataset = calc->calculate({ {1.003, 0.0073}, {1.003, 0.0073} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(7, dataset.getDataPoint({ 1.003, 0.0073 }));
    }
    {
        auto dataset = calc->calculate({ {-1.02, 0.901}, {-1.02, 0.901} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(1, dataset.getDataPoint({ -1.02, 0.901 }));
    }
    {
        auto dataset = calc->calculate({ {-10, 10}, {-10, 10} }, 0.1, { 0,0 });
        BOOST_CHECK_EQUAL(0, dataset.getDataPoint({ -10, 10 }));
    }
    {
        auto dataset = calc->calculate({ { -1,1 }, { 1,-1 } }, 2, { 0,0 });
        BOOST_CHECK_EQUAL(1, dataset.getDataPoint({ -1, -1 }));
        BOOST_CHECK_EQUAL(1, dataset.getDataPoint({ -1, 1 }));
        BOOST_CHECK_EQUAL(1, dataset.getDataPoint({ 1, -1 }));
        BOOST_CHECK_EQUAL(1, dataset.getDataPoint({ 1, 1 }));
    }
    delete calc; 
}

BOOST_FIXTURE_TEST_SUITE(TS_JuliaStrategy, JuliaFixture)

BOOST_FIXTURE_TEST_CASE(JuliaOneThreadCalculation, JuliaFixture)
{
    juliaTest(new JuliaCPUStrategy(settings));
}

BOOST_FIXTURE_TEST_CASE(JuliaMultiThreadCalculation, JuliaFixture)
{
    juliaTest(new JuliaCPUMultiStrategy(settings));
}

BOOST_FIXTURE_TEST_CASE(JuliaOpenCLStrategyCaclulaton, JuliaFixture)
{
    BOOST_REQUIRE(JuliaOCLStrategy::compile(&JuliaOCLStrategy::kernelCalculate, EasyOCL::OCLDevice::CPU()));
    juliaTest(new JuliaOCLStrategy(EasyOCL::OCLDevice::CPU(), settings));
}

BOOST_AUTO_TEST_SUITE_END();
#pragma warning(pop)