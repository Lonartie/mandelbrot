/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/24                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Mandelbrot.Core/ComplexPlaneMapper.h"
#pragma warning(push)
#pragma warning(disable:4305)

using namespace Mandelbrot::Core;

BOOST_AUTO_TEST_SUITE(TS_ComplexPlaneMapper);
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperSetVisibleRect_01, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   auto rect = ComplexRect({-1, 1}, {0,0});
//   auto size = ViewPoint(20, 20);
//   mapper.setVisibleRect(rect, size);
//   auto crect = mapper.calculateVisibleRect();
//   auto midpoint = mapper.getMidpoint();
//   BOOST_TEST_VIEWPOINT(midpoint, size);
//   BOOST_TEST_COMPLEXPOINT(rect.getUpperLeft(), crect.getUpperLeft());
//   BOOST_TEST_COMPLEXPOINT(rect.getLowerRight(), crect.getLowerRight());
//   BOOST_TEST(mapper.getZoom() == (precision_type) 0.05);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperSetVisibleRect_02, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   auto rect = ComplexRect({-1, 1}, {1,-1});
//   auto size = ViewPoint(20, 20);
//   mapper.setVisibleRect(rect, size);
//   auto crect = mapper.calculateVisibleRect();
//   auto midpoint = mapper.getMidpoint();
//   BOOST_TEST_VIEWPOINT(midpoint, QPointF(10,10));
//   BOOST_TEST_COMPLEXPOINT(rect.getUpperLeft(), crect.getUpperLeft());
//   BOOST_TEST_COMPLEXPOINT(rect.getLowerRight(), crect.getLowerRight());
//   BOOST_TEST(mapper.getZoom() == (precision_type) 0.1);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_01, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({100,100});
//   mapper.setZoom(1e-2);
//
//   ComplexPoint result = mapper.translate(ViewPoint{100,100});
//   ComplexPoint expected(0,0);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_02, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({100,100});
//   mapper.setZoom(1e-2);
//
//   ComplexPoint result = mapper.translate(ViewPoint{0,0});
//   ComplexPoint expected(-1, 1);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_03, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({100,100});
//   mapper.setZoom(1e-2);
//
//   ComplexPoint result = mapper.translate(ViewPoint{50, 50});
//   ComplexPoint expected(-0.5,0.5);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_04, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({100,100});
//   mapper.setZoom(1e-2);
//
//   ComplexPoint result = mapper.translate(ViewPoint{200, 0});
//   ComplexPoint expected(1,1);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_05, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({0,0});
//   mapper.setZoom(1e-1);
//
//   ComplexPoint result = mapper.translate(ViewPoint{0, 0});
//   ComplexPoint expected(0,0);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_06, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({0,0});
//   mapper.setZoom(1e-1);
//
//   ComplexPoint result = mapper.translate(ViewPoint{100, 100});
//   ComplexPoint expected(10,-10);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_07, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({50, 50});
//   mapper.setZoom(1e-1);
//
//   ComplexPoint result = mapper.translate(ViewPoint{50, 50});
//   ComplexPoint expected(0,0);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_08, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({50, 50});
//   mapper.setZoom(1e-1);
//
//   ComplexPoint result = mapper.translate(ViewPoint{0, 0});
//   ComplexPoint expected(-5,5);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperViewPointToComplexPoint_09, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({50, 50});
//   mapper.setZoom(0);
//
//   ComplexPoint result = mapper.translate(ViewPoint{100, 100});
//   ComplexPoint expected(1.17549435e-36, -1.17549435e-36);
//   BOOST_TEST_COMPLEXPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_01, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({10,10});
//   mapper.setZoom(1e-1);
//
//   ViewPoint result = mapper.translate(ComplexPoint{2, 1});
//   ViewPoint expected(30, 0);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_02, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({50,10});
//   mapper.setZoom(1e-1);
//
//   ViewPoint result = mapper.translate(ComplexPoint{2, -3});
//   ViewPoint expected(70, 40);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_03, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({50,25});
//   mapper.setZoom(1e-2);
//
//   ViewPoint result = mapper.translate(ComplexPoint{4.5, -3});
//   ViewPoint expected(500, 325);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_04, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({-10,25});
//   mapper.setZoom(1e-2);
//
//   ViewPoint result = mapper.translate(ComplexPoint{-6.5, 2.3});
//   ViewPoint expected(-660, -205);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_05, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({20.4, -82.1});
//   mapper.setZoom(1.5e-2);
//
//   ViewPoint result = mapper.translate(ComplexPoint{8.3,9.2});
//   ViewPoint expected(573.73337402343748, -695.43331298828127);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_06, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({-20.34, 200.5});
//   mapper.setZoom(1.9e-2);
//
//   ViewPoint result = mapper.translate(ComplexPoint{9.23, 9.02});
//   ViewPoint expected(465.44945922851565, -274.23687744140625);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_07, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({2389.1239, 123980.12347});
//   mapper.setZoom(1.1433e-1);
//
//   ViewPoint result = mapper.translate(ComplexPoint{1283.239, -12390.5});
//   ViewPoint expected(13613.116087500001, 232354.99846999999);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_08, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({92342.349, -1239.234892});
//   mapper.setZoom(5.123e-2);
//
//   ViewPoint result = mapper.translate(ComplexPoint{123012.3128, -1239.12389});
//   ViewPoint expected(2493519.5989999999, 22948.231904875);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexPointToViewPoint_09, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setMidpoint({-10,25});
//   mapper.setZoom(0);
//
//   ViewPoint result = mapper.translate(ComplexPoint{-6.5, 2.3});
//   ViewPoint expected(-2.764794231232625e+38, -9.7831178461528848e+37);
//
//   BOOST_TEST_VIEWPOINT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexRect_01, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setSize({100, 100});
//   mapper.setMidpoint({100, 100});
//   mapper.setZoom(1e-2);
//
//   ComplexRect result = mapper.calculateVisibleRect();
//   ComplexRect expected = ComplexRect({-1, 1}, {0, 0});
//
//   BOOST_TEST_COMPLEXRECT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexRect_02, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setSize({100, 100});
//   mapper.setMidpoint({0, 0});
//   mapper.setZoom(1e-2);
//
//   ComplexRect result = mapper.calculateVisibleRect();
//   ComplexRect expected = ComplexRect({0, 0}, {1, -1});
//
//   BOOST_TEST_COMPLEXRECT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexRect_03, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setSize({100, 100});
//   mapper.setMidpoint({0, 100});
//   mapper.setZoom(1e-2);
//
//   ComplexRect result = mapper.calculateVisibleRect();
//   ComplexRect expected = ComplexRect({0, 1}, {1, 0});
//
//   BOOST_TEST_COMPLEXRECT(expected, result);
//}
//
//BOOST_AUTO_TEST_CASE(ComplexPlaneMapperComplexRect_04, *boost::unit_test::tolerance<precision_type>(1e-5))
//{
//   ComplexPlaneMapper mapper;
//   mapper.setSize({100, 100});
//   mapper.setMidpoint({100, 0});
//   mapper.setZoom(1e-2);
//
//   ComplexRect result = mapper.calculateVisibleRect();
//   ComplexRect expected = ComplexRect({-1, 0}, {0, -1});
//
//   BOOST_TEST_COMPLEXRECT(expected, result);
//}

BOOST_AUTO_TEST_SUITE_END();
#pragma warning(pop)