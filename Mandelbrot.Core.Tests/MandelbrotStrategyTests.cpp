/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/04/05                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Mandelbrot.Core/MandelbrotCPUStrategy.h"
#include "Mandelbrot.Core/MandelbrotCPUMultiStrategy.h"
#include "Mandelbrot.Core/MandelbrotOCLStrategy.h"
#include "Mandelbrot.Core/NoSmoothingStrategy.h"
using namespace Mandelbrot::Core;
#pragma warning(push)
#pragma warning(disable:4305)

struct MandelbrotFixture
{
   MandelbrotFixture()
      : smooth(new NoSmoothingStrategy)
      , settings(128, *smooth)
   {}

   ~MandelbrotFixture()
   {
      delete smooth;
   }

   SmoothingStrategy* smooth;
   CalculationSettings settings;
};

void mandelbrotTest(CalculationStrategy* calc)
{
   {
      auto dataset = calc->calculate({{-1, 0.03}, {-1, 0.03}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(128, dataset.getDataPoint({-1, 0.03}));
   }
   {
      auto dataset = calc->calculate({{1, -0.23}, {1, -0.23}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(1, dataset.getDataPoint({1, -0.23}));
   }
   {
      auto dataset = calc->calculate({{0.003, -1.0073}, {0.003, -1.0073}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(8, dataset.getDataPoint({0.003, -1.0073}));
   }
   {
      auto dataset = calc->calculate({{1.003, 0.0073}, {1.003, 0.0073}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(1, dataset.getDataPoint({1.003, 0.0073}));
   }
   {
      auto dataset = calc->calculate({{-1.02, 0.901}, {-1.02, 0.901}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(2, dataset.getDataPoint({-1.02, 0.901}));
   }
   {
      auto dataset = calc->calculate({{-10, 10}, {-10, 10}}, 0.1, {0,0});
      BOOST_CHECK_EQUAL(0, dataset.getDataPoint({-10, 10}));
   }
   {
      auto dataset = calc->calculate({{ -1,1 }, { 1,-1 }}, 2, {0,0});
      BOOST_CHECK_EQUAL(2, dataset.getDataPoint({-1, -1}));
      BOOST_CHECK_EQUAL(2, dataset.getDataPoint({-1, 1}));
      BOOST_CHECK_EQUAL(1, dataset.getDataPoint({1, -1}));
      BOOST_CHECK_EQUAL(1, dataset.getDataPoint({1, 1}));
   }
   delete calc;
}

BOOST_FIXTURE_TEST_SUITE(TS_MandelbrotStrategy, MandelbrotFixture)


BOOST_FIXTURE_TEST_CASE(MandelbrotOneThreadCalculation, MandelbrotFixture)
{
   mandelbrotTest(new MandelbrotCPUStrategy(settings));
}

BOOST_FIXTURE_TEST_CASE(MandelbrotMultiThreadCalculation, MandelbrotFixture)
{
   mandelbrotTest(new MandelbrotCPUMultiStrategy(settings));
}

BOOST_FIXTURE_TEST_CASE(MandelbrotOpenCLStrategyCalculation, MandelbrotFixture)
{
   BOOST_REQUIRE(MandelbrotOCLStrategy::compile(&MandelbrotOCLStrategy::kernelCalculate, EasyOCL::OCLDevice::CPU()));
   mandelbrotTest(new MandelbrotOCLStrategy(EasyOCL::OCLDevice::CPU(), settings));
}

BOOST_AUTO_TEST_SUITE_END();
#pragma warning(pop)