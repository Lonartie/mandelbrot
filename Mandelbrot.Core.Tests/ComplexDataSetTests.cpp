/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/31                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#include "stdafx.h"
#include "Mandelbrot.Core/ComplexDataSet.h"
using namespace Mandelbrot::Core;
#pragma warning(push)
#pragma warning(disable:4305)

BOOST_AUTO_TEST_SUITE(TS_ComplexDataSet);

BOOST_AUTO_TEST_CASE(ComplexDataSetSizeCalculation)
{
   {
      ComplexDataSet set({{0, 0}, {10, 10}}, 1);
      BOOST_CHECK_EQUAL_QSIZE(QSize(11, 11), set.dim());
      BOOST_CHECK_EQUAL(121, set.size());
   }

   {
      ComplexDataSet set({{0, 0}, {10.9, 10.9}}, 1);
      BOOST_CHECK_EQUAL_QSIZE(QSize(11, 11), set.dim());
      BOOST_CHECK_EQUAL(121, set.size());
   }

   {
      ComplexDataSet set({{0, 0}, {10, 10}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(21, 21), set.dim());
      BOOST_CHECK_EQUAL(441, set.size());
   }

   {
      ComplexDataSet set({{0, 0}, {10.9, 10.9}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(22, 22), set.dim());
      BOOST_CHECK_EQUAL(484, set.size());
   }

   {
      ComplexDataSet set({{0, 0}, {10.5, 10.5}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(22, 22), set.dim());
      BOOST_CHECK_EQUAL(484, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {10, 10}}, 1);
      BOOST_CHECK_EQUAL_QSIZE(QSize(6, 6), set.dim());
      BOOST_CHECK_EQUAL(36, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {10.9, 10.9}}, 1);
      BOOST_CHECK_EQUAL_QSIZE(QSize(6, 6), set.dim());
      BOOST_CHECK_EQUAL(36, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {10, 10}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(11, 11), set.dim());
      BOOST_CHECK_EQUAL(121, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {10.9, 10.9}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(12, 12), set.dim());
      BOOST_CHECK_EQUAL(144, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {10.5, 10.5}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(12, 12), set.dim());
      BOOST_CHECK_EQUAL(144, set.size());
   }

   {
      ComplexDataSet set({{5, 5}, {5, 5}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(1, 1), set.dim());
      BOOST_CHECK_EQUAL(1, set.size());
   }

   {
      ComplexDataSet set({{0, 0}, {0, 0}}, 0.5);
      BOOST_CHECK_EQUAL_QSIZE(QSize(1, 1), set.dim());
      BOOST_CHECK_EQUAL(1, set.size());
   }

   {
      ComplexDataSet set({{1, 1}, {2, 2}}, 0);
      BOOST_CHECK_EQUAL_QSIZE(QSize(1, 1), set.dim());
      BOOST_CHECK_EQUAL(0, set.size());
   }

   {
      ComplexDataSet set({{1, 1}, {2, 2}}, 0);
      BOOST_CHECK_EQUAL_QSIZE(QSize(1, 1), set.dim());
      BOOST_CHECK_EQUAL(0, set.size());
   }
}

BOOST_AUTO_TEST_CASE(ComplexDataSetIndexCalculation)
{
   ComplexDataSet set({{0, 1}, {1, 0}}, 0.1); // --> 1 / 0.1 + 1 * 1 / 0.1 + 1 elements = 11 * 11 elements = 121 elements

   set.setDataPoint({0.0, 0.0}, 1);
   set.setDataPoint({1.0, 1.0}, 2);
   set.setDataPoint({0.5, 0.5}, 3);

   BOOST_CHECK_EQUAL(1, set.data()[110]);
   BOOST_CHECK_EQUAL(2, set.data()[ 10]);
   BOOST_CHECK_EQUAL(3, set.data()[ 60]);
}

BOOST_AUTO_TEST_SUITE_END();
#pragma warning(pop)