/********************************************************/
/*                      Mandelbrot                      */
/*                                                      */
/*  Author  :   Leon Gierschner                         */
/*  Date    :   2020/03/23                              */
/*                                                      */
/*  All rights reserved                                 */
/*                                                      */
/********************************************************/

#pragma once
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS

// Boost
#include <boost/test/unit_test.hpp>

// Mandelbrot.Core
#include "Mandelbrot.Core/stdafx.h"

// Mandelbrot.Core.Tests
#include "QtHelpers.h"
#include "ComplexPointHelper.h"